// Function to return the last element of an array without removing it
Array.prototype.last = function() {
  return this[this.length - 1];
}

var cnt = 0;
var secondRepeated = -1;
const chart = document.getElementById("chart");
const secondaryChart = document.getElementById("chartSecondary");

let history = new History();
let historyE = new History();
let historyD = new History();

// if(splitScreen)

function resetChart() {
  Plotly.purge(chart);
  if(!splitScreen) Plotly.purge(secondaryChart);
}

function splitChart() {
  splitScreen = !splitScreen;
  historyE.clear();
  historyD.clear();
  resetChart();
  // Remove chart titles
  removeChartTitle();
  // Build charts
  buildChart(chartType);

  // Add chart titles
  insertChartTitle();
}

function insertChartTitle() {
  if(splitScreen) {
    $(chart).prepend(`<div class="sideTitle">Left side</div>`);
    $(secondaryChart).prepend(`<div class="sideTitle">Right side</div>`);
  }
}

function removeChartTitle() {
  $(chart).html("");
  $(secondaryChart).html("");
}


function insertNextDataChart() {
  // Prevent inserting data for repeated seconds
  if(totalSeconds === secondRepeated) {
    return;
  }
  secondRepeated = totalSeconds;
  // Save current values in the history objects
    // Carnivores
    history.carnivores.population.push(popC.no_div)
    history.carnivores.velocity.push(velMedC.no_div)
    history.carnivores.agility.push(forcaMedC.no_div)
    history.carnivores.radius.push(raioMedC.no_div)
    history.carnivores.detection.push(raioDetMedC.no_div)
    history.carnivores.energy.push(energMedC.no_div)
    history.carnivores.expenditure.push(taxaEnergMedC.no_div)
    history.carnivores.average_nest_size.push(ninhadaMediaC.no_div)

    // Herbivores
    history.herbivores.population.push(popH.no_div)
    history.herbivores.velocity.push(velMedH.no_div)
    history.herbivores.agility.push(forcaMedH.no_div)
    history.herbivores.radius.push(raioMedH.no_div)
    history.herbivores.detection.push(raioDetMedH.no_div)
    history.herbivores.energy.push(energMedH.no_div)
    history.herbivores.expenditure.push(taxaEnergMedH.no_div)
    history.herbivores.average_nest_size.push(ninhadaMediaH.no_div)

    // Seconds
    history.seconds.push(totalSeconds)

    // Other information for data analysis
    history.food_rate.push(inputFoodRate.value)

    if(splitScreen) {
      // Left side history
        // Carnivores
        historyE.carnivores.population.push(popC.left)
        historyE.carnivores.velocity.push(velMedC.left)
        historyE.carnivores.agility.push(forcaMedC.left)
        historyE.carnivores.radius.push(raioMedC.left)
        historyE.carnivores.detection.push(raioDetMedC.left)
        historyE.carnivores.energy.push(energMedC.left)
        historyE.carnivores.expenditure.push(taxaEnergMedC.left)
        historyE.carnivores.average_nest_size.push(ninhadaMediaC.left)

        // Herbivores
        historyE.herbivores.population.push(popH.left)
        historyE.herbivores.velocity.push(velMedH.left)
        historyE.herbivores.agility.push(forcaMedH.left)
        historyE.herbivores.radius.push(raioMedH.left)
        historyE.herbivores.detection.push(raioDetMedH.left)
        historyE.herbivores.energy.push(energMedH.left)
        historyE.herbivores.expenditure.push(taxaEnergMedH.left)
        historyE.herbivores.average_nest_size.push(ninhadaMediaH.left)

        historyE.seconds.push(totalSeconds)

        historyE.food_rate.push(inputFoodRate.value)

      // Right side history
        // Carnivores
        historyD.carnivores.population.push(popC.right)
        historyD.carnivores.velocity.push(velMedC.right)
        historyD.carnivores.agility.push(forcaMedC.right)
        historyD.carnivores.radius.push(raioMedC.right)
        historyD.carnivores.detection.push(raioDetMedC.right)
        historyD.carnivores.energy.push(energMedC.right)
        historyD.carnivores.expenditure.push(taxaEnergMedC.right)
        historyD.carnivores.average_nest_size.push(ninhadaMediaC.right)

        // Herbivores
        historyD.herbivores.population.push(popH.right)
        historyD.herbivores.velocity.push(velMedH.right)
        historyD.herbivores.agility.push(forcaMedH.right)
        historyD.herbivores.radius.push(raioMedH.right)
        historyD.herbivores.detection.push(raioDetMedH.right)
        historyD.herbivores.energy.push(energMedH.right)
        historyD.herbivores.expenditure.push(taxaEnergMedH.right)
        historyD.herbivores.average_nest_size.push(ninhadaMediaH.right)

        historyD.seconds.push(totalSeconds)

        historyD.food_rate.push(inputFoodRate.value)
    }
  // ------------------------------------------------------------
  // Use the same time array for all charts
  let arraySeconds = [[totalSeconds], [totalSeconds]];

  // Identify the current chart
  let values;
  let values2;

  switch(chartType) {
    case 1:
      if(splitScreen) {
        values = [[historyE.carnivores.population.last()], [historyE.herbivores.population.last()]];
        values2 = [[historyD.carnivores.population.last()], [historyD.herbivores.population.last()]];
      } else
        values = [[history.carnivores.population.last()], [history.herbivores.population.last()]];
      break;
    case 2:
      if(splitScreen) {
        values = [[historyE.carnivores.velocity.last()], [historyE.herbivores.velocity.last()]];
        values2 = [[historyD.carnivores.velocity.last()], [historyD.herbivores.velocity.last()]];
      } else
        values = [[history.carnivores.velocity.last()], [history.herbivores.velocity.last()]];
      break;
    case 3:
      if(splitScreen) {
        values = [[historyE.carnivores.agility.last()], [historyE.herbivores.agility.last()]];
        values2 = [[historyD.carnivores.agility.last()], [historyD.herbivores.agility.last()]];
      } else
        values = [[history.carnivores.agility.last()], [history.herbivores.agility.last()]];
      break;
    case 4:
      if(splitScreen) {
        values = [[historyE.carnivores.radius.last()], [historyE.herbivores.radius.last()]];
        values2 = [[historyD.carnivores.radius.last()], [historyD.herbivores.radius.last()]];
      } else
        values = [[history.carnivores.radius.last()], [history.herbivores.radius.last()]];
      break;
    case 5:
      if(splitScreen) {
        values = [[historyE.carnivores.detection.last()], [historyE.herbivores.detection.last()]];
        values2 = [[historyD.carnivores.detection.last()], [historyD.herbivores.detection.last()]];
      } else
        values = [[history.carnivores.detection.last()], [history.herbivores.detection.last()]];
      break;
    case 6:
      if(splitScreen) {
        values = [[historyE.carnivores.energy.last()], [historyE.herbivores.energy.last()]];
        values2 = [[historyD.carnivores.energy.last()], [historyD.herbivores.energy.last()]];
      } else
        values = [[history.carnivores.energy.last()], [history.herbivores.energy.last()]];
      break;
    case 7:
      if(splitScreen) {
        values = [[historyE.carnivores.expenditure.last()], [historyE.herbivores.expenditure.last()]];
        values2 = [[historyD.carnivores.expenditure.last()], [historyD.herbivores.expenditure.last()]];
      } else
        values = [[history.carnivores.expenditure.last()], [history.herbivores.expenditure.last()]];
      break;
    case 8:
      if(splitScreen) {
        values = [[historyE.carnivores.average_nest_size.last()], [historyE.herbivores.average_nest_size.last()]];
        values2 = [[historyD.carnivores.average_nest_size.last()], [historyD.herbivores.average_nest_size.last()]];
      } else
        values = [[history.carnivores.average_nest_size.last()], [history.herbivores.average_nest_size.last()]];
  }

  Plotly.extendTraces(chart, {y: values, x: arraySeconds}, [0, 1]);
  cnt++;
  
  if (cnt > 500) {
      Plotly.relayout('chart', {
          xaxis: {
              range: [cnt - 500, cnt]
          }
      });
  }
  
  if (splitScreen) {
    Plotly.extendTraces(chartSecundario, {y: values2, x: arraySeconds}, [0, 1]);
    cnt++;
  
    if (cnt > 500) {
        Plotly.relayout('chartSecundario', {
            xaxis: {
                range: [cnt - 500, cnt]
            }
        });
    }
  }
} 
function changeChart(type) {
  if (type == chartType || type < 1 || type > 8) {
    return;
  }
  resetChart();
  removeChartTitle();
  buildChart(type);
  insertChartTitle();
  chartType = type;
}
  
function buildChart(type) {
  // INDEX OF WHICH DATA TO FETCH FROM THE DATA ARRAY (relative to carnivores)
  // let index = 1;                  // Chart 1 values as initializer
  let title = "Population";
  let yTitle = "Number of Individuals"; // Y-axis title
  let data = [];
  let data2 = [];
  
  // Fetch data history for the new chart type -----------------------------------------------------
  switch (type) {
    case 1: // Population
      if (splitScreen) {
        data = [historyE.carnivores.population, historyE.herbivores.population];
        data2 = [historyD.carnivores.population, historyD.herbivores.population];
      } else {
        data = [history.carnivores.population, history.herbivores.population];
      }
      break;
    case 2: // Velocity
        // index = 3;
      title = "Velocity";
      yTitle = "Average Velocity";
  
      if (splitScreen) {
        data = [historyE.carnivores.velocity, historyE.herbivores.velocity];
        data2 = [historyD.carnivores.velocity, historyD.herbivores.velocity];
      } else {
        data = [history.carnivores.velocity, history.herbivores.velocity];
      }
      break;
    case 3: // Agility
              // index = 5;
      title = "Agility";
      yTitle = "Average Agility";
  
      if (splitScreen) {
        data = [historyE.carnivores.agility, historyE.herbivores.agility];
        data2 = [historyD.carnivores.agility, historyD.herbivores.agility];
      } else {
        data = [history.carnivores.agility, history.herbivores.agility];
      }
      break;
    case 4: // Radius
              // index = 7;
      title = "Radius";
      yTitle = "Average Radius";
  
      if (splitScreen) {
        data = [historyE.carnivores.radius, historyE.herbivores.radius];
        data2 = [historyD.carnivores.radius, historyD.herbivores.radius];
      } else {
        data = [history.carnivores.radius, history.herbivores.radius];
      }
      break;
    case 5: // Detection Radius
              // index = 9;
      title = "Detection Range";
      yTitle = "Average Detection Radius";
  
      if (splitScreen) {
        data = [historyE.carnivores.detection, historyE.herbivores.detection];
        data2 = [historyD.carnivores.detection, historyD.herbivores.detection];
      } else {
        data = [history.carnivores.detection, history.herbivores.detection];
      }
      break;
    case 6: // Energy
              // index = 11;
      title = "Energy";
      yTitle = "Average Energy Level";
  
      if (splitScreen) {
        data = [historyE.carnivores.energy, historyE.herbivores.energy];
        data2 = [historyD.carnivores.energy, historyD.herbivores.energy];
      } else {
        data = [history.carnivores.energy, history.herbivores.energy];
      }
      break;
    case 7: // Energy Expenditure
              // index = 13;
      title = "Energy Expenditure";
      yTitle = "Average Energy Expenditure Rate";
  
      if (splitScreen) {
        data = [historyE.carnivores.expenditure, historyE.herbivores.expenditure];
        data2 = [historyD.carnivores.expenditure, historyD.herbivores.expenditure];
      } else {
        data = [history.carnivores.expenditure, history.herbivores.expenditure];
      }
      break;
    case 8: // Average Nest Size
              // index = 15;
      title = "Average Nest Size";
      yTitle = "Average Nest Size";
  
      if (splitScreen) {
        data = [historyE.carnivores.average_nest_size, historyE.herbivores.average_nest_size];
        data2 = [historyD.carnivores.average_nest_size, historyD.herbivores.average_nest_size];
      } else {
        data = [history.carnivores.average_nest_size, history.herbivores.average_nest_size];
      }
 
    }
  

  // -----------------------------------------------------------------------------------------------

// INSERTS THE ENTIRE DATA HISTORY INTO THE CHART
  let carnivores = {
    x: splitScreen ? historicalE.seconds : historical.seconds,
    y: data[0],
    type: 'scatter',
    mode: 'lines',
    name: 'Carnivores',
    line: { color: 'red', shape: 'spline' }
  };

  let herbivores = {
    x: splitScreen ? historicalE.seconds : historical.seconds,
    y: data[1],
    type: 'scatter',
    mode: 'lines',
    name: 'Herbivores',
    line: { color: 'green', shape: 'spline' }
  };

  let dataConfig = [carnivores, herbivores];

  var layout = {
  title: title,

  xaxis: {
      showline: true,
      domain: [0],
      title: "Seconds",
      showgrid: true
    },
  yaxis: {
      showline: true,
      title: yTitle,
      rangemode: "tozero"
  },
  legend: {
      orientation: 'h',
          traceorder: 'reversed',
      x: 0.05,
      y: -.3
  },
  plot_bgcolor: "#222",
  paper_bgcolor: "#222",
  font: {
      color: '#ddd'
  }
}

  Plotly.newPlot('chart', dataConfig, layout);

  let carnivores2, herbivores2, dataConfig2;
  if (splitScreen) {
    carnivores2 = {
      x: historicalD.seconds,
      y: data2[0],
      type: 'scatter',
      mode: 'lines',
      name: 'Carnivores',
      line: { color: 'red', shape: 'spline' }
    };

    herbivores2 = {
      x: historicalD.seconds,
      y: data2[1],
      type: 'scatter',
      mode: 'lines',
      name: 'Herbivores',
      line: { color: 'green', shape: 'spline' }
    };

    dataConfig2 = [carnivores2, herbivores2];

    Plotly.newPlot(chartSecundario, dataConfig2, layout);
  }
}