class Rectangle {
    constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    // Checks if the point is contained within its boundaries (borders)
    containsPoint(point) {
        return (
            point.position.x >= this.x - this.w &&
            point.position.x <= this.x + this.w &&
            point.position.y >= this.y - this.h &&
            point.position.y <= this.y + this.h
        );
    }

    // Method to check if rectangles intersect
    intersectsR(range) {
        return !(
            range.x - range.w > this.x + this.w ||
            range.x + range.w < this.x - this.w ||
            range.y - range.h > this.y + this.h ||
            range.y + range.h < this.y - this.h
        );
    }

    // Method to check if the rectangle intersects with a circle
    intersectsC(circle) {
        // Temporary variables to set edges for testing
        let testX = circle.x;
        let testY = circle.y;

        // Which edge is closest?
        if (circle.x < this.x - this.w) {
            testX = this.x - this.w;
        } else if (circle.x > this.x + this.w) {
            testX = this.x + this.w;
        }

        if (circle.y < this.y - this.h) {
            testY = this.y - this.h;
        } else if (circle.y > this.y + this.h) {
            testY = this.y + this.h;
        }

        // Get distance from closest edges
        let distX = circle.x - testX;
        let distY = circle.y - testY;
        let distance = Math.sqrt(distX * distX + distY * distY);

        // If the distance is less than or equal to the radius, collision!
        if (distance <= circle.r) {
            return true;
        }
        return false;
    }
}
