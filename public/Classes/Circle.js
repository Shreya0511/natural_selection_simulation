class Circle {
    constructor(x, y, r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    // Checks if the point is contained within its boundaries (borders)
    containsPoint(point) {
        let xPoint = point.position.x;
        let yPoint = point.position.y;

        return (
            Math.sqrt(Math.pow(xPoint - this.x, 2) + Math.pow(yPoint - this.y, 2)) <= this.r // If the distance from the point to the circle is less than or equal to the radius
        );
    }

    // Method to check if the rectangles intersect
    intersects(range) {
        return !( // If this expression is true, they do NOT intersect
            range.x - range.width > this.x + this.width ||
            range.x + range.width < this.x - this.width ||
            range.y - range.height > this.y + this.height ||
            range.y + range.height < this.y - this.height
        );
    }
}
