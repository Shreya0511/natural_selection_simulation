class Infos {
    constructor() {
        this.population = [];
        this.speed = [];
        this.agility = [];
        this.radius = [];
        this.detection = [];
        this.energy = [];
        this.expenditure = [];
        this.average_nest_size = [];
    }

    clear() {
        this.population.length = 0;
        this.speed.length = 0;
        this.agility.length = 0;
        this.radius.length = 0;
        this.detection.length = 0;
        this.energy.length = 0;
        this.expenditure.length = 0;
        this.average_nest_size.length = 0;
    }
}
