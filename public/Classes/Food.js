class Food {
    static foods = []; // An array to store all instances of the Food class
    static id = 0; // A static variable to assign unique IDs to each instance
  
    constructor(x, y, radius) {
      this.position = new Vector(x, y); // The position of the food
      this.radius = radius; // The radius of the food
  
      // The energy of the food is proportional to its area
      this.foodEnergy = Math.floor(Math.PI * Math.pow(this.radius, 2)) * 15;
  
      Food.foods.push(this); // Add the current instance to the array
  
      this.id = Food.id++; // Assign a unique ID to the current instance
    }
  
    display() {
      // Display the food on the canvas
      c.beginPath();
      c.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
      c.fillStyle = "rgb(115, 158, 115)";
      c.fill();
    }
  
    checId(id) {
      // Check if the given ID matches the ID of the current instance
      return id == this.id;
    }
  }
  