class DNA {
    constructor(initialRadius, maxVelocity, maxForce, color, initialDetectionRadius, breedingInterval, sex) {
      this.initialRadius = initialRadius;
      this.maxVelocity = maxVelocity;
      this.maxForce = maxForce;
      this.color = color;
      this.initialDetectionRadius = initialDetectionRadius;
      this.breedingInterval = breedingInterval;
      this.sex = sex; // string that can be XX (female) or XY (male)
    }
  
    mutate() {
      var mutatedDNA;
  
      // Initial radius
      var mutatedInitialRadius = newMutation(this.initialRadius);
      if (mutatedInitialRadius < 0) {
        mutatedInitialRadius = 0;
      }
      // Maximum velocity
      var mutatedMaxVelocity = newMutation(this.maxVelocity);
      if (mutatedMaxVelocity < 0) {
        mutatedMaxVelocity = 0;
      }
  
      // Maximum force
      var mutatedMaxForce = newMutation(this.maxForce);
  
      // Color
      var mutatedColor = colorMutation(this.color);
  
      // Initial detection radius
      var mutatedInitialDetectionRadius = newMutation(this.initialDetectionRadius);
      if (mutatedInitialDetectionRadius < mutatedInitialRadius) {
        mutatedInitialDetectionRadius = mutatedInitialRadius;
      }
  
      // Breeding interval
      var mutatedBreedingInterval = breedingMutation(this.breedingInterval[0], this.breedingInterval[1]);
  
      mutatedDNA = new DNA(
        mutatedInitialRadius,
        mutatedMaxVelocity,
        mutatedMaxForce,
        mutatedColor,
        mutatedInitialDetectionRadius,
        mutatedBreedingInterval
      );
  
      return mutatedDNA;
    }
  }
  