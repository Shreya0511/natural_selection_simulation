class Organism {
    static organisms = [];
    static totalOrganisms = 0;
    static id = 0;
    constructor(x, y, dna, parent = null) {
        this.id = Organism.id++;
        this.position = new Vector(x, y);
        if (parent) {
            this.parent = parent;
        }
        this.children = [];
    
        this.initialRadius = dna.initialRadius;
        this.maxVelocity = dna.maxVelocity;
        this.maxForce = dna.maxForce;
        this.color = dna.color;
        this.initialDetectionRadius = dna.initialDetectionRadius;
        this.breedingInterval = dna.breedingInterval;
        this.sex = dna.sex;
    
        // DNA -> Object to separate only the attributes passed to descendants
        this.dna = new DNA(
            this.initialRadius,
            this.maxVelocity,
            this.maxForce,
            this.color,
            this.initialDetectionRadius,
            this.breedingInterval,
            this.sex
        );
    
        this.radius = this.initialRadius;
        this.velocity = new Vector(0.0001, 0.0001);
        this.acceleration = new Vector(0, 0);
        var rgb = this.color.substring(4, this.color.length - 1).split(",");
        this.color2 = "rgba(" + Math.floor(parseInt(rgb[0]) * 0.4) + "," + Math.floor(parseInt(rgb[1]) * 0.4) + "," + Math.floor(parseInt(rgb[2]) * 0.4) + ")";
    
        this.detectionRadius = this.initialDetectionRadius;
        this.maxEnergy = Math.pow(this.radius, 2) * 6;
        this.fixedMaxEnergy = Math.pow(this.initialRadius * 1.5, 2) * 6; // Used to obtain non-variable values in the graph
    
        // BREEDING
    
        // this.energy = this.maxEnergy * 0.75
        if (parent) {
            this.energy = (this.maxEnergy * (0.75 + Math.random() / 4)) / (parent.offspringSize) * 0.6; // Starts with a fraction of the maximum energy
        } else {
            this.energy = this.maxEnergy * 0.75;
        }
    
        this.energySpendingRate;
        this.minimumExpenditure = 0.0032 * Math.pow(Math.pow(this.radius, 2), 0.75); // Following Kleiber's law for the metabolic rate of living organisms
        this.maximumEnergySpendingRate = this.minimumExpenditure + (Math.pow(this.initialRadius * 1.5, 2) * Math.pow(this.maxVelocity, 2)) * 0.00012;
        this.reproductionChance = 0.5;
        this.status;
        this.foodQuantity = 0;
        this.timesReproduced = 0;
        this.secondBirth = totalSeconds; // "totalSeconds" is the global variable
        this.lifespan = parseInt(generateNumberInRange(200, 300)); // lifespan of the organism
        this.lifetime = 0;
        this.offspringSize;
    
        // Status variables
        this.eating = false;
        this.fleeing = false;
        this.wandering = false;
    
        // Variable that defines the distance from the edge at which organisms will start curving to avoid collisions with the borders
        this.d = 20;
    
        // Variables used for the wander() method
        this.dCircle = 2;
        this.circleRadius = 1;
        this.wanderAngle = Math.random() * 360;
    
        // Variable to separate organisms born before screen division from those born after
        this.beforeDivision = false;
        this.temporaryFixedPosition = new Vector(x, y);
    
        Organism.organisms.push(this);
        Organism.totalOrganisms++;
    }
    // Creating a common reproduction method for all organisms
    _reproduce() {
        return this.dna.mutate();
    }
    // Method to update the state of the organism
    update() {
        this.lifetime = totalSeconds - this.secondBirth;
        this.energySpendingRate = (Math.pow(this.radius, 2) * Math.pow(this.velocity.mag(), 2)) * 0.0002; // Update based on current velocity
        // Energy decrease rate
        if (this.energy > 0) {
            this.energy -= (this.energySpendingRate + this.minimumExpenditure);

            if (Math.random() < (0.0005 * this.foodQuantity) / 10) { // Low number as it is checked every frame. Higher chances with more food eaten
                if (Math.random() <= this.reproductionChance) {
                    // this.reproduce();
                    // BREEDING

                    this.offspringSize = generateInteger(this.breedingInterval[0], this.breedingInterval[1] + 1);
                    for (var i = 0; i < this.offspringSize; i++) {
                        if (Math.random() < 0.2) { // To space out the births
                            this.reproduce();
                        }
                    }
                }
            }
        } else {
            this.die();
        }

        if (this.lifetime >= this.lifespan) { // if more time has passed since birth than the organism's lifespan
            this.die();
        }

        // Border confinement
        if (screenDivided) {
            this.createBorders(true);
        } else {
            this.createBorders(false);
        }

        // Update velocity (add velocity vector to acceleration vector)
        this.velocity.add(this.acceleration);
        // Limit velocity
        this.velocity.limit(this.maxVelocity);

        // console.log("velocity angle: ", this.velocity.headingDegs());

        // If there is a proxy, update position through it to monitor position changes
        if (this.proxy) {
            this.proxy.add(this.velocity);
        } else {
            // Velocity changes the position (just like acceleration changes velocity)
            this.position.add(this.velocity);
        }
        // Reset acceleration to 0 every cycle
        this.acceleration.mul(0);

        this.display();
    }

    increaseSize() {
        if (this.radius < (this.initialRadius * 1.5)) {
            this.radius += 0.05 * this.radius;
            this.detectionRadius += 0.03 * this.detectionRadius;
        }
        this.maxEnergy = Math.pow(this.radius, 2) * 6;
    }

    // Method to create borders that delimit the organism's space
    createBorders(screenDivided) { // screenDivided: boolean
        this.defineBorders(screenDivided);
        this.avoidBorders(screenDivided);
    }

    
  
    

    // Method to prevent organisms from going outside the screen
    defineBorders(screenDivided) {
        if (screenDivided == true) {
        // Borders for organisms on the left side
            if (this.position.x <= universeWidth / 2) {
                if (this.position.x + 2 * this.radius > universeWidth / 2) // right border (the division, i.e., the middle of the screen)
                    this.velocity.x = this.velocity.x * -1; // invert the x velocity if it exceeds the border
                if (this.position.x < 0) // left border
                    this.velocity.x = this.velocity.x * -1;

                if (this.position.y + this.radius > universeHeight) // bottom border
                    this.velocity.y = this.velocity.y * -1;

                if (this.position.y < 0) // top border
                    this.velocity.y = this.velocity.y * -1;
            } else { // Borders for organisms on the right side
                if (this.position.x + 2 * this.radius > universeWidth) // right border
                    this.velocity.x = this.velocity.x * -1; //invert the x velocity if it exceeds the canvas border

                if (this.position.x - this.radius < universeWidth / 2) // left border (the division, i.e., the middle of the screen)
                    this.velocity.x = this.velocity.x * -1;

                if (this.position.y + this.radius > universeHeight) // bottom border
                    this.velocity.y = this.velocity.y * -1;

                if (this.position.y < 0) // top border
                    this.velocity.y = this.velocity.y * -1;
            }

        } else { // if the screen is NOT divided
            if (this.position.x + 2 * this.radius > universeWidth) // right border
                this.velocity.x = this.velocity.x * -1; //invert the x velocity if it exceeds the canvas border

            if (this.position.x - this.radius < 0) // left border
                this.velocity.x = this.velocity.x * -1;

            if (this.position.y + this.radius > universeHeight) // bottom border
                this.velocity.y = this.velocity.y * -1;

            if (this.position.y < 0) // top border
                this.velocity.y = this.velocity.y * -1;
            }

        }

        // Method to apply a force to the organism that prevents it from continuing in a trajectory outside the screen
        avoidBorders(screenDivided) {
            var desiredVelocity = null; // This velocity will be the vector that indicates where the organism should go to avoid going beyond the border
            this.nearBorder = false;

            if (screenDivided == true) {
            // For organisms on the left side
                if (this.position.x <= universeWidth / 2) {
                // Left border
                if (this.position.x - this.radius < this.d) { // d is an attribute of every organism that delimits the distance from a border from which it will start maneuvering
                    desiredVelocity = new Vector(this.maxVelocity, this.velocity.y); // Make its velocity maximum in the x direction (to the right)
                    this.nearBorder = true;
                }
            // Right border
                else if (this.position.x + 2 * this.radius > universeWidth / 2 - this.d) { // the right border is the canvas half (in divided screen)
                    desiredVelocity = new Vector(-this.maxVelocity, this.velocity.y); // Make its velocity maximum in the -x direction (to the left)
                    this.nearBorder = true;
                }
                // Top border
                if (this.position.y - this.radius < this.d) {
                    desiredVelocity = new Vector(this.velocity.x, this.maxVelocity); // Make its velocity maximum in the y direction (downwards)
                    this.nearBorder = true;
                }
                // Bottom border
                else if (this.position.y + this.radius > universeHeight - this.d) {
                    desiredVelocity = new Vector(this.velocity.x, -this.maxVelocity); // Make its velocity maximum in the -y direction (upwards)
                    this.nearBorder = true;
                }
            }

            // For organisms on the right side
            else {
                // Left border
                if (this.position.x - this.radius < universeWidth / 2 + this.d) { // the left border is the canvas half (in divided screen)
                    desiredVelocity = new Vector(this.maxVelocity, this.velocity.y); // Make its velocity maximum in the x direction (to the right)
                    this.nearBorder = true;
                }
                // Right border
                else if (this.position.x + 2 * this.radius > universeWidth - this.d) {
                    desiredVelocity = new Vector(-this.maxVelocity, this.velocity.y); // Make its velocity maximum in the -x direction (to the left)
                    this.nearBorder = true;
                }
                // Top border
                if (this.position.y < this.d) {
                    desiredVelocity = new Vector(this.velocity.x, this.maxVelocity); // Make its velocity maximum in the y direction (downwards)
                    this.nearBorder = true;
                }
                // Bottom border
                else if (this.position.y > universeHeight - this.d) {
                    desiredVelocity = new Vector(this.velocity.x, -this.maxVelocity); // Make its velocity maximum in the -y direction (upwards)
                    this.nearBorder = true;
                }
            }

        } else { // if the screen is NOT divided
                // Left border
            if (this.position.x - this.radius < this.d) {
                desiredVelocity = new Vector(this.maxVelocity, this.velocity.y); // Make its velocity maximum in the x direction (to the right)
                this.nearBorder = true;
            }
                // Right border
            else if (this.position.x + this.radius > universeWidth - this.d) {
                desiredVelocity = new Vector(-this.maxVelocity, this.velocity.y); // Make its velocity maximum in the -x direction (to the left)
                this.nearBorder = true;
            }
                // Top border
            if (this.position.y - this.radius < this.d) {
                desiredVelocity = new Vector(this.velocity.x, this.maxVelocity); // Make its velocity maximum in the y direction (downwards)
                this.nearBorder = true;
            }
                // Bottom border
            else if (this.position.y + this.radius > universeHeight - this.d) {
                desiredVelocity = new Vector(this.velocity.x, -this.maxVelocity); // Make its velocity maximum in the -y direction (upwards)
                this.nearBorder = true;
            }
        }


        if (desiredVelocity != null) { // If any of the previous conditions are satisfied
            desiredVelocity.normalize(); // Normalize (scale to length 1) the desiredVelocity vector
            desiredVelocity.mul(this.maxVelocity); // Multiply the vector (which now has length 1) by the maximum velocity
            var redirection = desiredVelocity.sub(this.velocity); // Create a force vector that will redirect the organism
            redirection.limit(this.maxForce * 100); // Limit this force with a greater margin to give it a chance to be greater than other forces acting on it
            this.applyForce(redirection); // Apply this force to the organism and make it slightly stronger to prioritize it over other forces
        }
    }
    // Method to apply a force that makes the organism turn towards the nearest target naturally
    applyForce(force) {
    // Add the force to the acceleration, increasing it
    // Mass can also be considered in the calculation: A = F / M (not implemented)
        this.acceleration.add(force);
    }
  
    // Test for implementing behavior learning
    behavior(good, bad) {
        // Implementation pending
    }
  
  // Method that makes the organism wander around when not fleeing or pursuing
    wander() {
        // if (!this.fleeing && !this.pursuing) {
        this.wandering = true;
        this.status = "wandering";
        // The idea is to create a small force each frame just ahead of the organism, at a distance of d.
        // We will draw a circle in front of the organism, and the displacement force vector will originate from the center
        // of the circle and have a magnitude equal to its radius. Thus, the larger the circle, the stronger the force.
        // To determine the front of the organism, we will use the velocity vector as it always points in the direction of the organism's movement.
    
        // CREATING THE CIRCLE
        var circleCenter = new Vector(0, 0); // Create a vector that represents the distance from the organism to the center of the circle
        circleCenter = this.velocity.copy(); // This ensures that the circle is exactly in front of the organism (as explained above)
        circleCenter.normalize(); // Normalize the vector, so its magnitude is now 1 (instead of the magnitude of the velocity vector as it was on the previous line)
        circleCenter.multiply(this.circleDistance); // The variable circleDistance is a globally defined constant that holds the value of the distance from the center of the circle
  
        // CREATING THE DISPLACEMENT FORCE
        var displacement = new Vector(0, -1);
        displacement.multiply(this.circleRadius); // The force will have a magnitude equal to the radius of the circle
        // Randomly change the direction of the force
        displacement.rotateDegs(this.wanderAngle); // Rotate the force by wanderAngle (a variable defined in the constructor)
        // Slightly change the value of wanderAngle so that it gradually changes over time
        this.wanderAngle += Math.random() * 30 - 15; // Changes by a value between -15 and 15
    
        // CREATING THE WANDER FORCE
        // The wander force can be thought of as a vector that starts from the organism's position and goes to a point
        // on the circumference of the circle we created
        // Now that the circle center and the displacement force vectors have been created, we simply add them
        // to create the wander force
        var wanderForce = new Vector(0, 0);
        wanderForce = circleCenter.add(displacement);
    
        if (this.eating || this.fleeing) { // Decrease the wander force when eating or fleeing to prioritize these tasks
            wanderForce.multiply(0.03);
            }
            this.applyForce(wanderForce.multiply(0.2));
            // }
    }
  
    // Method that calculates the redirection force towards a target
    // REDIRECTION = DESIRED VELOCITY - VELOCITY
    pursue(target) {
        target.fleeing = true;
        // The desired velocity vector is the target's position vector minus the own position vector
        var desiredVelocity = target.position.subNew(this.position); // A vector pointing from the organism's location to the target
        // Scale the desired velocity to the maximum velocity
        desiredVelocity.setMagnitude(this.maxVelocity);
    
        // Redirection = desired velocity - velocity
        var redirection = desiredVelocity.subNew(this.velocity);
        redirection.limit(this.maxForce); // Limit the redirection to the maximum force
    
        // Add the redirection force to the acceleration
        this.applyForce(redirection);
    }
  
    // Method for sex-reproductive behavior to search for nearby partners
    searchForPartners() {
        // Implementation pending
    }
    
    // Method for sex-reproductive behavior to approach the found partner
    approachPartner() {
        // CALL THE combinaDnas() METHOD HERE
    }
  
    // Method for sex-reproductive behavior to randomly choose genes from the father and mother
    combineDNA(partner) {
        var childDNA = [];
    
        // Initial radius
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.initialRadius);
        } else {
            childDNA.push(partner.dna.initialRadius);
        }
    
        // Maximum velocity
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.maxVelocity);
        } else {
            childDNA.push(partner.dna.maxVelocity);
        }
    
        // Maximum force
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.maxForce);
        } else {
            childDNA.push(partner.dna.maxForce);
        }
    
        // Color
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.color);
        } else {
            childDNA.push(partner.dna.color);
        }
    
        // Initial detection radius
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.initialDetectionRadius);
        } else {
            childDNA.push(partner.dna.initialDetectionRadius);
        }
    
        // Breeding interval
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.breedingInterval);
        } else {
            childDNA.push(partner.dna.breedingInterval);
        }
    
        // Sex
        if (Math.random() < 0.5) {
            childDNA.push(this.dna.sex);
        } else {
            childDNA.push(partner.dna.sex);
        }
    
        var childDNAObj = new DNA(childDNA[0], childDNA[1], childDNA[2], childDNA[3], childDNA[4], childDNA[5], childDNA[6]);
        
        return childDNAObj;
    }
    
    isDead() {
        return this.energy <= 0;
    }
    
    remove(list) {
        var what, a = arguments, L = a.length, index;
        while (L > 1 && list.length) {
            what = a[--L];
            while ((index = list.indexOf(what)) !== -1) {
                list.splice(index, 1);
            }
        }
        return list;
    }
    
    checkId(id) {
        return (id == this.id);
    }
}
    