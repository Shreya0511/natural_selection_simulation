class Carnivore extends Organism {
    // Static properties
    static carnivores = [];
    static highlight = false;
  
    // Constructor
    constructor(x, y, dna, parent = null) {
      super(x, y, dna, parent); // Calling the constructor of the parent class
  
      // Variable to count when a carnivore can reproduce
      this.reproductionCountdown = 0;
  
      // Adding the carnivore instance to the carnivores array
      Carnivore.carnivores.push(this);
    }
  
    // Reproduction method (with mutations)
    reproduce() {
      // Increasing the reproduction count
      this.reproductionCount++;
  
      // Calling the _reproduce() method to get the DNA of the child organism
      var childDNA = this._reproduce();
  
      // Creating a new Carnivore instance with the child's DNA
      var child = new Carnivore(
        this.position.x,
        this.position.y,
        childDNA,
        this
      );
  
      // Adding the child organism to the parent's children array
      this.children.push(child);
  
      return child; // Returning the newly created child organism
    }
  
    // Sexually reproduction method with another carnivore as the partner
    reproduceSexually(partner) {
      // Increasing the reproduction count
      this.reproductionCount++;
  
      // Combining the DNA of the current carnivore with the partner's DNA
      var childDNA = this.combineDNAs(partner);
  
      // Creating a new Carnivore instance with the combined DNA
      var child = new Carnivore(
        this.position.x,
        this.position.y,
        childDNA,
        this
      );
  
      // Adding the child organism to the parent's children array
      this.children.push(child);
  
      return child; // Returning the newly created child organism
    }
  
    // Method to handle the death of the carnivore
    die() {
      // Removing the carnivore from the carnivores array
      Carnivore.carnivores = super.remove(Carnivore.carnivores, this);
  
      // Removing the carnivore from the organisms array
      Organism.organisms = super.remove(Organism.organisms, this);
    }
  
    // Method for carnivores to search and hunt for herbivores
    huntHerbivore(quadtree, visionCone) {
      // Setting the status of the carnivore to "hunting"
      this.status = "hunting";
  
      // Flag to indicate if the carnivore is currently eating
      this.eating = false;
  
      // Variable record: the smallest distance (the record) to a herbivore so far
      var record = Infinity;
  
      // The index of the closest herbivore in the nearby herbivores list
      var closestIndex = -1;
  
      // Finding herbivores within the carnivore's vision cone using the quadtree
      let nearbyHerbivores = quadtree.findHerbivores(visionCone);
  
      // Looping through each herbivore in the nearbyHerbivores list
      for (var i = nearbyHerbivores.length - 1; i >= 0; i--) {
        // Calculating the squared distance between the carnivore and the current herbivore
        var d2 =
          Math.pow(this.position.x - nearbyHerbivores[i].position.x, 2) +
          Math.pow(this.position.y - nearbyHerbivores[i].position.y, 2);
  
        // Checking if the squared distance is smaller than the current record
        if (d2 <= record) {
          record = d2; // Updating the record distance
          closestIndex = i; // Updating the index of the closest herbivore
        }
      }
  
      // If the closest herbivore is within the detection radius
      if (record <= Math.pow(this.detectionRadius, 2)) {
        this.eating = true; // Setting the eating flag to true
        this.wandering = false; // Setting the wandering flag to false
        this.status = "hunting";
  
        // Setting the closest herbivore to be fleeing
        nearbyHerbivores[closestIndex].fleeing = true;
        nearbyHerbivores[closestIndex].eating = false;
        nearbyHerbivores[closestIndex].wandering = false;
        nearbyHerbivores[closestIndex].status = "fleeing";
  
        // If the distance to the closest herbivore is within a certain threshold
        if (record <= 25) {
          this.consumeHerbivore(nearbyHerbivores[closestIndex]);
        } else if (nearbyHerbivores.length != 0) {
          this.pursue(nearbyHerbivores[closestIndex]);
        }
      }
    }
  
    // Method for the carnivore to consume a herbivore
    consumeHerbivore(herbivore) {
      this.foodCount++;
  
      // Energy absorption when consuming the herbivore
      // If the energy gained from the herbivore (10% of its total energy)
      // is less than the remaining energy needed to fill the energy bar,
      // it is added in full (the 10%)
      if (this.maxEnergy - this.energy >= herbivore.maxEnergy * 0.1) {
        this.energy += herbivore.maxEnergy * 0.1;
      } else {
        this.energy = this.maxEnergy;
      }
  
      // Limiting the energy to not exceed the maximum energy
      if (this.energy > this.maxEnergy) {
        this.energy = this.maxEnergy;
      }
  
      // The consumed herbivore dies (removed from the herbivores list)
      herbivore.die();
  
      // Increasing the size of the carnivore
      this.increaseSize();
    }
  
    // Method to display the carnivore on the canvas
    display() {
      c.beginPath();
      c.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2);
  
      // Setting the fill and stroke styles based on whether highlighting is enabled
      if (Herbivore.highlight) {
        c.fillStyle = "rgba(" + this.color2.substr(5).replace(")", "") + ",0.15)";
        c.strokeStyle = "rgba(" + this.color.substr(4).replace(")", "") + ",0.15)";
      } else {
        c.fillStyle = this.color2;
        c.strokeStyle = this.color;
      }
  
      c.lineWidth = 5;
      c.stroke();
      c.fill();
  
      // Drawing the detection radius (if needed)
      // c.beginPath();
      // c.arc(this.position.x, this.position.y, this.detectionRadius, 0, Math.PI * 2);
      // c.strokeStyle = "grey";
      // c.stroke();
    }
  }
  