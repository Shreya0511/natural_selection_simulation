class QuadTree {
    constructor(limits, capacity) {
        this.limits = limits; // Attribute of type Rectangle
        this.capacity = capacity; // Threshold value for subdivision
        this.points = [];
        this.food = [];
        this.herbivores = [];
        this.carnivores = [];
        // this.organisms = this.food.concat(this.herbivores, this.carnivores); // Array containing all the food, herbivores, and carnivores within its root
        this.divided = false;
    }
  
    // Subdivides the QuadTree into 4 child rectangles
    subdivide() {
        let x = this.limits.x;
        let y = this.limits.y;
        let w = this.limits.w;
        let h = this.limits.h;
    
        let ne = new Rectangle(x + w / 2, y - h / 2, w / 2, h / 2);
        this.northeast = new QuadTree(ne, this.capacity);
    
        let nw = new Rectangle(x - w / 2, y - h / 2, w / 2, h / 2);
        this.northwest = new QuadTree(nw, this.capacity);
    
        let se = new Rectangle(x + w / 2, y + h / 2, w / 2, h / 2);
        this.southeast = new QuadTree(se, this.capacity);
    
        let sw = new Rectangle(x - w / 2, y + h / 2, w / 2, h / 2);
        this.southwest = new QuadTree(sw, this.capacity);
    
        this.divided = true;
    }
  
    insertPoint(point) {
        if (!this.limits.containsPoint(point)) { // Checks if the point is within the boundaries of the root rectangle
            return false;
        }
    
        if (this.points.length < this.capacity) {
            this.points.push(point);
            return true;
        } else {
            if (!this.divided) { // The QuadTree does not subdivide if it has already been divided
            this.subdivide();
            }
    
            // We do not check the location of the point here as it will be checked at the beginning of each recursive call
            if (this.northeast.insertPoint(point)) {
            return true;
            } else if (this.northwest.insertPoint(point)) {
            return true;
            } else if (this.southeast.insertPoint(point)) {
            return true;
            } else if (this.southwest.insertPoint(point)) {
            return true;
            }
        }
    }
  
    insertFood(food) {
        if (!this.limits.containsPoint(food)) { // Checks if the food is within the boundaries of the root rectangle
            return false;
        }
    
        if (this.food.length < this.capacity) {
            this.food.push(food);
            return true;
        } else {
            if (!this.divided) { // The QuadTree does not subdivide if it has already been divided
            this.subdivide();
            }
  
        // We do not check the location of the food here as it will be checked at the beginning of each recursive call
            if (this.northeast.insertFood(food)) {
                return true;
            } else if (this.northwest.insertFood(food)) {
                return true;
            } else if (this.southeast.insertFood(food)) {
                return true;
            } else if (this.southwest.insertFood(food)) {
                return true;
            };
        }
    }
    insertHerbivore(herbivore) {
        if (!this.limits.containsPoint(herbivore)) { // Checks if the herbivore is within the boundaries of the root rectangle
          return false;
        }
      
        if (this.herbivores.length < this.capacity) { // If there is still capacity for herbivores
          this.herbivores.push(herbivore); // Inserts the herbivore into its list
          return true;
        } else {
            if (!this.divided) { // The QuadTree does not subdivide if it has already been divided
                this.subdivide();
            }
        
            // We do not check the location of the herbivore here as it will be checked at the beginning of each recursive call
            if (this.northeast.insertHerbivore(herbivore)) {
                return true;
            } else if (this.northwest.insertHerbivore(herbivore)) {
                return true;
            } else if (this.southeast.insertHerbivore(herbivore)) {
                return true;
            } else if (this.southwest.insertHerbivore(herbivore)) {
                return true;
            };
        }
      }
      
    insertCarnivore(carnivore) {
        if (!this.limits.containsPoint(carnivore)) { // Checks if the carnivore is within the boundaries of the root rectangle
          return false;
        }
      
        if (this.carnivores.length < this.capacity) { // If there is still capacity for carnivores
          this.carnivores.push(carnivore); // Inserts the carnivore into its list
          return true;
        } else {
            if (!this.divided) { // The QuadTree does not subdivide if it has already been divided
                this.subdivide();
            }
        
            // We do not check the location of the carnivore here as it will be checked at the beginning of each recursive call
            if (this.northeast.insertCarnivore(carnivore)) {
                return true;
            } else if (this.northwest.insertCarnivore(carnivore)) {
                return true;
            } else if (this.southeast.insertCarnivore(carnivore)) {
                return true;
            } else if (this.southwest.insertCarnivore(carnivore)) {
                return true;
            };
        }
    }
      
      searchPoints(range, foundPoints) { // range is of type Rectangle
        if (!foundPoints) {
          foundPoints = [];
        }
      
        if (!this.limits.intersects(range)) { // If they do NOT intersect, do not execute the code
          return;
        } else { // If they intersect
            for (let point of this.points) { // For the points in this QuadTree
                if (range.containsPoint(point)) { // If the point belongs to the "range" rectangle
                    foundPoints.push(point);
                }
            }
        
            if (this.divided) { // If the QuadTree has child QuadTrees
                this.northwest.searchPoints(range, foundPoints);
                this.northeast.searchPoints(range, foundPoints);
                this.southwest.searchPoints(range, foundPoints);
                this.southeast.searchPoints(range, foundPoints);
            }
      
            return foundPoints;
        }
    }
    searchFood(circle, foundFood) {
        if (!foundFood) {
          foundFood = [];
        }
      
        if (!this.limits.intersectsCircle(circle)) { // If they do NOT intersect, do not execute the code
          return foundFood;
        } else { // If they intersect
            for (let food of this.foods) { // For the foods in this QuadTree
                if (circle.containsPoint(food)) { // If the food belongs to the circle
                    foundFood.push(food);
                }
            }
      
            if (this.divided) { // If the QuadTree has child QuadTrees
                this.northwest.searchFood(circle, foundFood);
                this.northeast.searchFood(circle, foundFood);
                this.southwest.searchFood(circle, foundFood);
                this.southeast.searchFood(circle, foundFood);
            }
      
            return foundFood;
        }
    }
      
    searchHerbivores(circle, foundHerbivores) {
            if (!foundHerbivores) {
                foundHerbivores = [];
            }
      
            if (!this.limits.intersectsCircle(circle)) { // If they do NOT intersect, do not execute the code
                return foundHerbivores;
            } else { // If they intersect
            for (let herbivore of this.herbivores) { // For the herbivores in this QuadTree
                if (circle.containsPoint(herbivore)) { // If the herbivore belongs to the circle
                    foundHerbivores.push(herbivore);
                }
            }
        
            if (this.divided) { // If the QuadTree has child QuadTrees
                this.northwest.searchHerbivores(circle, foundHerbivores);
                this.northeast.searchHerbivores(circle, foundHerbivores);
                this.southwest.searchHerbivores(circle, foundHerbivores);
                this.southeast.searchHerbivores(circle, foundHerbivores);
            }
      
            return foundHerbivores;
        }
    }
      
    searchCarnivores(circle, foundCarnivores) {
        if (!foundCarnivores) {
          foundCarnivores = [];
        }
      
        if (!this.limits.intersectsCircle(circle)) { // If they do NOT intersect, do not execute the code
          return foundCarnivores;
        } else { // If they intersect
          for (let carnivore of this.carnivores) { // For the carnivores in this QuadTree
                if (circle.containsPoint(carnivore)) { // If the carnivore belongs to the circle
                    foundCarnivores.push(carnivore);
                }
        }
      
          if (this.divided) { // If the QuadTree has child QuadTrees
                this.northwest.searchCarnivores(circle, foundCarnivores);
                this.northeast.searchCarnivores(circle, foundCarnivores);
                this.southwest.searchCarnivores(circle, foundCarnivores);
                this.southeast.searchCarnivores(circle, foundCarnivores);
            }
      
            return foundCarnivores;
        }
    }
      
    draw() {
        c.beginPath();
        c.rect(this.limits.x - this.limits.w, this.limits.y - this.limits.h, this.limits.w * 2, this.limits.h * 2);
        c.stroke();
        
        if (this.divided) {
            this.northeast.draw();
            this.northwest.draw();
            this.southeast.draw();
            this.southwest.draw();
        }
    }
    
}