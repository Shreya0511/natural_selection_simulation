// Class for constructing vectors
class Vector {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    // Resets the x and y values of the vector to the specified values
    set(x, y) {
        this.x = x;
        this.y = y;
    };

    // Returns the squared magnitude of the vector
    magSq() {
        var x = this.x, y = this.y;
        return x * x + y * y;
    };

    // Returns the magnitude of the vector
    mag() {
        return Math.sqrt(this.magSq());
    };

    // Adds a specified vector to the current vector and returns the vector itself (updated), not a new one
    add(v) {
        this.x += v.x;
        this.y += v.y;
        return this;
    };

    // Subtracts a specified vector from the current vector and returns the vector itself (updated), not a new one
    sub(v) {
        this.x -= v.x;
        this.y -= v.y;
        return this;
    };

    // Subtracts a specified vector from the current vector and returns a new vector
    subNew(v) {
        var x = this.x - v.x;
        var y = this.y - v.y;
        return new Vector(x, y);
    };

    // Returns this vector after dividing it by a specified value
    // Used to decrease the size of a vector. If n is two, the vector will have half the size
    div(n) {
        this.x /= n;
        this.y /= n;
        return this;
    };

    // Returns this vector after multiplying it by a specified value
    // Used to increase the size of a vector. If n is two, the vector will have twice the size
    mul(n) {
        this.x *= n;
        this.y *= n;
        return this;
    };

    // Changes the size of the vector to 1 (this is called normalizing a vector)
    normalize() {
        // Divides the vector itself by the magnitude returned in mag(), resulting in 1
        return this.div(this.mag());
    };

    // Changes the size of the vector to a specified value
    setMag(n) {
        // Normalizes (changes the size to 1) and then multiplies by n
        return this.normalize().mul(n);
    };


    // Returns the distance between two points (defined by x and y of a vector v)
    dist(v) {
        var d = v.copy().sub(this);
        return d.mag();
    }
    
    // Limits the vector's magnitude to a specified limit (used to limit speed, for example)
    limit(l) {
        var mSq = this.magSq();
        if (mSq > l * l) {
        this.div(Math.sqrt(mSq));
        this.mul(l);
        }
        return this;
    }
    
    // Returns the direction to which the vector is pointing (in radians)
    headingRads() {
        var h = Math.atan2(this.y, this.x);
        return h;
    }
    
    // Returns the direction to which the vector is pointing (in degrees)
    headingDegs() {
        var r = Math.atan2(this.y, this.x);
        var h = (r * 180.0) / Math.PI;
        return h;
    }
    
    // Rotates the vector by 'a' radians
    // This can be used to make the drawing of the character rotate to always align with its movement
    rotateRads(a) {
        var newHead = this.headingRads() + a;
        var mag = this.mag();
        this.x = Math.cos(newHead) * mag;
        this.y = Math.sin(newHead) * mag;
        return this;
    }
    
    // Rotates the vector by 'a' degrees
    rotateDegs(a) {
        a = (a * Math.PI) / 180.0;
        var newHead = this.headingRads() + a;
        var mag = this.mag();
        this.x = Math.cos(newHead) * mag;
        this.y = Math.sin(newHead) * mag;
        return this;
    }
    
    // Returns the angle between two vectors (in degrees) --> /\
    angleBetweenDegs(x, y) {
        var r = this.angleBetweenRads(x, y);
        var d = (r * 180) / Math.PI;
        return d;
    }
    
    // Checks if two vectors are identical and returns a boolean
    equals(x, y) {
        var a, b;
        if (x instanceof Vector) {
        a = x.x || 0;
        b = x.y || 0;
        } else {
        a = x || 0;
        b = y || 0;
        }
    
        return this.x === a && this.y === b;
    }
    
    // Returns a copy of this vector
    copy() {
        return new Vector(this.x, this.y);
    }
}