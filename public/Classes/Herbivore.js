class Herbivore extends Organism {
    static herbivores = [];
    static highlight = false;

    constructor(x, y, dna, parent = null) {
        super(x, y, dna, parent);

        // Variable to count when a herbivore can reproduce
        this.reproductionCount = 0;

        Herbivore.herbivores.push(this);
    }

    // Reproduction method (with mutations)
    reproduce() {
        this.reproductionCount++;

        var childDNA = this._reproduce();
        var child = new Herbivore(
            this.position.x, this.position.y, childDNA, this
        );

        this.children.push(child);

        return child;
    }

    reproduceSexually(partner) {
        this.reproductionCount++;

        var childDNA = this.combineDNAs(partner);
        var child = new Herbivore(
            this.position.x, this.position.y, childDNA, this
        );

        this.children.push(child);

        return child;
    }

    die() {
        if (this.popoverId) deletePopover(this.popoverId, this.id);
        Herbivore.herbivores = super.remove(Herbivore.herbivores, this);
        Organism.organisms = super.remove(Organism.organisms, this);
    }
    searchFood(qtree, visionH) {
        this.status = "searching for food";
        this.eating = false;
        // Variable 'record': the smallest distance (record) to a food so far
        var record = Infinity; // Initially, we set this distance as infinity
        var nearestIndex = -1; // The index of the nearest food so far
        
        // Get a list of nearby foods from the QuadTree
        let nearbyFoods = qtree.searchFoods(visionH); // searchFoods() returns a list of foods

        for (var i = nearbyFoods.length - 1; i >= 0; i--) {
            // Distance 'd' between this organism and the current food being analyzed in the list
            // var d = this.position.dist(nearbyFoods[i].position);

            var d2 = Math.pow(this.position.x - nearbyFoods[i].position.x, 2) + Math.pow(this.position.y - nearbyFoods[i].position.y, 2);

            if (d2 <= record) { // If the distance is smaller than the record distance,
                record = d2; // update the record to 'd'
                nearestIndex = i; // update the nearest food index to 'i'
            }
        }
        
        // Time to eat!
        if (record <= Math.pow(this.detectionRadius, 2)) {
            this.eating = true;
            this.roaming = false;
            this.status = "getting food";
            
            if (record <= 25) { // Since record is the squared distance, we compare it with 5 squared (5^2 = 25)
                
                let staticListIndex = 0;

                // Loop to find the food with the id of the nearest food and delete it from the static list based on its id
                Food.foods.every(food => {
                    if (food.checkId(nearbyFoods[nearestIndex].id)) {
                        return false;
                    }
                    staticListIndex++;

                    return true;
                });

                this.eatFood(nearbyFoods[nearestIndex], staticListIndex);

                ///////////////////////////////////////////////////////////////////////////////
                // this.reproductionCount++;

                // if (this.reproductionCount >= 3) { // If the herbivore eats <reproductionCount> foods
                //     if (Math.random() < this.reproductionChance) { // chance of reproduction
                //         this.reproduce();
                //     }
                //     this.reproductionCount = 0; // Reset the variable to be able to reproduce again
                // }
                //////////////////////////////////////////////////////////////////////////////////////////
                
            } else if (nearbyFoods.length != 0) {
                this.pursue(nearbyFoods[nearestIndex]);
            }
        }
    }

    eatFood(food, index) {
        this.foodQuantity++;
        // Energy absorption when eating food:
        // If the energy of the food is less than the amount needed to fill the energy bar,
        // the herbivore acquires the entire energy of the food
        if (this.maxEnergy - this.energy >= food.energyFood * 0.1) {
            this.energy += food.energyFood * 0.1;
        } else {
            this.energy = this.maxEnergy; // Limit the energy to not exceed the maximum energy
        }
        if (this.energy > this.maxEnergy) {
            this.energy = this.maxEnergy;
        }
        Food.foods.splice(index, 1); // Remove the food from the list of foods
        this.increaseSize();
    }

    // Method to detect a predator (similar to searchFood())
    // The method finds the nearest carnivore, and if it is within the detection radius,
    // it triggers the flee() method
    detectPredator(qtree, visionH) {
        this.fleeing = false;
        // Variable 'record': the smallest distance (record) to a carnivore so far
        var record = Infinity; // Initially, we set this distance as infinity
        var nearestIndex = -1; // The index of the nearest carnivore so far

        // Get a list of carnivores in the QuadTree within its vision range
        let nearbyCarnivores = qtree.searchCarnivores(visionH); // searchCarnivores() returns a list of carnivores

        // Loop through each carnivore in the list of nearby carnivores
        for (var i = nearbyCarnivores.length - 1; i >= 0; i--) {
            // Distance 'd' between this organism and the current carnivore being analyzed in the list
            // var d = this.position.dist(nearbyCarnivores[i].position);

            var d2 = Math.pow(this.position.x - nearbyCarnivores[i].position.x, 2) + Math.pow(this.position.y - nearbyCarnivores[i].position.y, 2);

            if (d2 <= record) { // If the distance is smaller than the record distance,
                record = d2; // update the record to 'd'
                nearestIndex = i; // update the nearest carnivore index to 'i'
            }
        }

        // Time to flee!
        if (record <= Math.pow(this.detectionRadius, 2)) {
            if (nearbyCarnivores.length != 0) {
                // Call the flee() method, which changes the herbivore's velocity to the opposite direction of the predator
                this.flee(nearbyCarnivores[nearestIndex]);
            }
        }
    }

    // Method that updates the velocity (and thus the position) of the herbivore to make it move in the opposite direction
    // of the carnivore
    flee(target) {
        // The desired velocity vector is the target's position vector minus the herbivore's position vector
        var desiredVelocity = target.position.subNew(this.position); // A vector pointing from the herbivore's location to the target
        // Currently, the desired velocity is pointing towards the carnivore. So, we need to invert the values
        // of x and y of the vector to make it point in the opposite direction. It's like in a Cartesian plane: if we invert x,
        // the line is vertically mirrored. If we invert y, it's horizontally mirrored. If we invert both,
        // the line is diametrically opposite, i.e., it points exactly in the opposite direction.
        desiredVelocity.x = -desiredVelocity.x; // Invert x
        desiredVelocity.y = -desiredVelocity.y; // Invert y
        // Scale the desired velocity to the herbivore's maximum velocity
        desiredVelocity.setMag(this.maxVelocity);

        // Steering = desired velocity - velocity. It represents the force that will be applied to the herbivore
        // to change its velocity direction
        var steering = desiredVelocity.subNew(this.velocity);
        steering.limit(this.maxForce); // Limit the steering force to the maximum force

        // Add the steering force to the acceleration
        this.applyForce(steering);
    }
    display() {
        c.beginPath();
        c.ellipse(this.position.x, this.position.y, this.radius * 0.7, this.radius * 1.1, this.velocity.headingRads() - Math.PI / 2, 0, Math.PI * 2);
        if (Carnivore.highlight) {
            c.fillStyle = "rgba(" + this.color.substr(4).replace(")", "") + ",0.15)";
            c.strokeStyle = "rgba(" + this.color.substr(4).replace(")", "") + ",0.15)";
        } else {
            c.fillStyle = this.color;
            c.strokeStyle = this.color;
        }
    
        c.fill();
    }
}