// Define the screen dimensions
// if(innerWidth<=425){
    var screen = { width: innerWidth, height: innerHeight - 8 };
    // }else{
    //var screen = {width: innerWidth - 500, height: innerHeight - 8}
    //}
    
    // Get the canvas element and set its dimensions
const canvas = document.querySelector("canvas");
canvas.width = screen.width;
canvas.height = screen.height;
    
const context = canvas.getContext('2d');
    
    // ---------------------------------------ZOOM IN / PANNING--------------------------------------------------
    
var universeSize = 3;
    
var universeWidth = canvas.width * universeSize; 
var universeHeight = canvas.height * universeSize; 
    
trackTransforms(context);
    
function redraw() {
    // Clear the entire canvas
    var p1 = context.transformedPoint(0, 0);
    var p2 = context.transformedPoint(canvas.width, canvas.height);
    context.clearRect(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y);
    
    context.save();
    if (!CanvasRenderingContext2D.prototype.resetTransform) {
        CanvasRenderingContext2D.prototype.resetTransform = function() {
            this.setTransform(1, 0, 0, 1, 0, 0);
        };
    }
        // context.setTransform(1, 0, 0, 1, 0, 0);
    context.clearRect(0, 0, universeWidth, universeHeight);
    context.restore();
}
    
var lastX = canvas.width / 2, lastY = canvas.height / 2;
    
var dragStart, dragged;
    
canvas.addEventListener('mousedown', function(evt) {
    if (evt.button == 1) {
        document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
        lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
        lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
        dragStart = context.transformedPoint(lastX, lastY);
        dragged = false;
    }
}, false);
    
canvas.addEventListener('mousemove', function(evt) {
    lastX = evt.offsetX || (evt.pageX - canvas.offsetLeft);
    lastY = evt.offsetY || (evt.pageY - canvas.offsetTop);
    dragged = true;
    if (dragStart) {
        var pt = context.transformedPoint(lastX, lastY);
        context.translate(pt.x - dragStart.x, pt.y - dragStart.y);
        redraw();
    }
    drawEverything();
}, false);
    
canvas.addEventListener('mouseup', function(evt) {
    dragStart = null;
}, false);
    
var scaleFactor = 1.05;
    
var zoom = function(clicks) {
    var pt = context.transformedPoint(lastX, lastY);
    context.translate(pt.x, pt.y);
    var factor = Math.pow(scaleFactor, clicks);
    context.scale(factor, factor);
    context.translate(-pt.x, -pt.y);
    redraw();
    drawEverything();
};
    


// Function to handle scrolling event
var handleScroll = function(evt){
    // Calculate the scroll delta based on different properties of the event object
    var delta = evt.wheelDelta ? evt.wheelDelta/40 : evt.detail ? -evt.detail : 0;
    if (delta){
        // If delta is non-zero, call the zoom function with the delta value
        zoom(delta);
    }
    return evt.preventDefault() && false;
};

// Add event listeners for scroll events on the canvas
canvas.addEventListener('DOMMouseScroll', handleScroll, false);
canvas.addEventListener('mousewheel', handleScroll, false);


// Adds additional transformation functionality to the canvas context
function trackTransforms(context){
    // Create an SVG element and get the SVG matrix
    var svg = document.createElementNS("http://www.w3.org/2000/svg",'svg');
    var transformMatrix = svg.createSVGMatrix();
    // Add the getTransform method to the canvas context, which returns the SVG matrix
    context.getTransform = function(){ return transformMatrix; };

    var savedTransforms = [];
    
    // Override the save method to save the current transformation matrix
    var save = context.save;
    context.save = function(){
        savedTransforms.push(transformMatrix.translate(0,0));
        return save.call(context);
    };

    // Override the restore method to restore the last saved transformation matrix
    var restore = context.restore;
    context.restore = function(){
        transformMatrix = savedTransforms.pop();
        return restore.call(context);
    };

    // Override the scale method to update the transformation matrix
    var scale = context.scale;
    context.scale = function(sx, sy){
        transformMatrix = transformMatrix.scaleNonUniform(sx, sy);
        return scale.call(context, sx, sy);
    };

    // Override the rotate method to update the transformation matrix
    var rotate = context.rotate;
    context.rotate = function(radians){
        transformMatrix = transformMatrix.rotate(radians * 180 / Math.PI);
        return rotate.call(context, radians);
    };

    // Override the translate method to update the transformation matrix
    var translate = context.translate;
    context.translate = function(dx, dy){
        transformMatrix = transformMatrix.translate(dx, dy);
        return translate.call(context, dx, dy);
    };

    // Override the transform method to update the transformation matrix
    var transform = context.transform;
    context.transform = function(a, b, c, d, e, f){
        var matrix2 = svg.createSVGMatrix();
        matrix2.a = a; matrix2.b = b; matrix2.c = c; matrix2.d = d; matrix2.e = e; matrix2.f = f;
        transformMatrix = transformMatrix.multiply(matrix2);
        return transform.call(context, a, b, c, d, e, f);
    };

    // Override the setTransform method to update the transformation matrix
    var setTransform = context.setTransform;
    context.setTransform = function(a, b, c, d, e, f){
        transformMatrix.a = a;
        transformMatrix.b = b;
        transformMatrix.c = c;
        transformMatrix.d = d;
        transformMatrix.e = e;
        transformMatrix.f = f;
        return setTransform.call(context, a, b, c, d, e, f);
    };

    var point = svg.createSVGPoint();
    // Add the transformedPoint method to the canvas context, which applies the transformation matrix to a point
    // and returns the transformed point
    context.transformedPoint = function(x, y){
        point.x = x; point.y = y;
        return point.matrixTransform(transformMatrix.inverse());
    }
}




// ------------------------------------------------------------------------------------------------------------------





var hunger_c = 0.8; // Percentage of maximum energy above which they won't eat (carnivores)
var hunger_h = 0.8; // Percentage of maximum energy above which they won't eat (herbivores)

var changeGraph = false;

// Variables for the graph (herbivores)
var populationH;
var averageVelocityH;
var averageStrengthH;
var averageRadiusH;
var averageDetectionRadiusH;
var averageEnergyH;
var averageEnergyConsumptionRateH;

// Variables for the graph (carnivores)
var populationC;
var averageVelocityC;
var averageStrengthC;
var averageRadiusC;
var averageDetectionRadiusC;
var averageEnergyC;
var averageEnergyConsumptionRateC;

// Variables for mutation changes
// var mutationProbability = labelProb; // Chances of each gene (attribute) to undergo mutation
var mutationMagnitude = 0.1; // Magnitude of mutation (how much it will vary)

var rightSideEmpty = true;
var leftSideEmpty = true;

// QuadTree
let canvasRectangle = new Rectangle(universeWidth/2, universeHeight/2, universeWidth/2, universeHeight/2);

var popoverId = 1;

// Edited organism configurations
var confC;
var confH;

// ---------------------------------------------------------------------------------------
//                                  FUNÇÕES
// ---------------------------------------------------------------------------------------

// Function to create the universe
function createUniverse(universeSize) {
    universeWidth = canvas.width * universeSize; 
    universeHeight = canvas.height * universeSize;
}

// Function to check mutation bias
function checkMutationBias(value, iterations) {
    var smaller = 0;
    var larger = 0;
    var equal = 0;
    var newValue = 0;
    for (var i = 0; i < iterations; i++) {
        newValue = newMutation(value);
        if (newValue > value) {
            larger++;
        } else if (newValue < value) {
            smaller++;
        } else {
            equal++;
        }
    }

    console.log("Larger: " + ((larger * 100) / iterations) + "%");
    console.log("Smaller: " + ((smaller * 100) / iterations) + "%");
    console.log("Equal: " + ((equal * 100) / iterations) + "%");
    console.log("Mutations: " + (((larger + smaller) * 100) / iterations) + "%");
}

// Function to check breeding mutation bias
function checkBreedingMutationBias(minBreedingInterval, maxBreedingInterval, iterations) {
    var smaller = 0;
    var larger = 0;
    var equal = 0;
    var breedingIntervalAvg = (minBreedingInterval + maxBreedingInterval) / 2;
    for (var i = 0; i < iterations; i++) {
        newInterval = breedingMutation(minBreedingInterval, maxBreedingInterval);
        newBreedingIntervalAvg = (newInterval[0] + newInterval[1]) / 2;
        if (newBreedingIntervalAvg > breedingIntervalAvg) {
            larger++;
        } else if (newBreedingIntervalAvg < breedingIntervalAvg) {
            smaller++;
        } else {
            equal++;
        }
    }

    console.log("Larger: " + ((larger * 100) / iterations) + "%");
    console.log("Smaller: " + ((smaller * 100) / iterations) + "%");
    console.log("Equal: " + ((equal * 100) / iterations) + "%");
    console.log("Mutations: " + (((larger + smaller) * 100) / iterations) + "%");
}

// Function to redraw all elements without animating
function redrawEverything() {
    Food.foods.forEach(food => {
        food.display();
    });
    Organism.organisms.forEach(organism => {
        organism.display();
    });
}

function exportToCsv(filename, rows) {
    var processRow = function (row) {
        var finalVal = '';
        for (var j = 0; j < row.length; j++) {
            var innerValue = row[j] === null ? '' : row[j].toString();
            if (row[j] instanceof Date) {
                innerValue = row[j].toLocaleString();
            }
            var result = innerValue.replace(/""/g, '""""');
            result = result.replace(".", ",");
            if (result.search(/("|,|\n)/g) >= 0)
                result = '"' + result + '"';
            if (j > 0)
                finalVal += ';';
            finalVal += result;
        }
        return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < rows.length; i++) {
        csvFile += processRow(rows[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement("a");
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
}

function createObjects(numCarnivores, numHerbivores, numFood) {
    for (var i = 0; i < numCarnivores; i++) {
        var x = (Math.random() * (universeWidth - 50) + 25);
        var y = (Math.random() * (universeHeight - 50) + 25);
        generateCarnivore(x, y);
    }
    for (var i = 0; i < numHerbivores; i++) {
        var x = (Math.random() * (universeWidth - 50) + 25);
        var y = (Math.random() * (universeHeight - 50) + 25);
        generateHerbivore(x, y);
    }
    for (var i = 0; i < numFood; i++) {
        var x = (Math.random() * (universeWidth - 50) + 25);
        var y = (Math.random() * (universeHeight - 50) + 25);
        generateFood(x, y);
    }
}


function destroyObjects() {
    Carnivore.carnivores.length = 0;
    Herbivore.herbivores.length = 0;
    Food.foods.length = 0;
    // changeFoodInterval(1001);
}

// Create more food over time
// The setInterval() function allows it to call the loop every x milliseconds
var foodRateInterval;

// Auxiliary variables for implementing screen division
var checkbox_division = document.getElementById('division');
var dividedScreen;
var loopLimit = 0;

function generateFood(x, y) {
    var radius = generateNumberInRange(1, 2);
    return new Food(x, y, radius);
}

function generateCarnivore(x, y) {
    var initialRadius = generateNumberInRange(3, 8);
    var maxVelocity = generateNumberInRange(1, 2.2);
    var maxForce = generateNumberInRange(0.01, 0.05);
    var color = generateColor();
    var initialDetectionRadius = generateNumberInRange(40, 120);
    var minOffspring = generateInteger(1, 1);
    var maxOffspring = minOffspring + generateInteger(1, 8);
    var offspringRange = [minOffspring, maxOffspring];
    var sex;

    if (Math.random() < 0.5) {
        sex = 'XX';
    } else {
        sex = 'XY';
    }

    if (conf_c) {
        initialRadius = conf_c.initialRadius;
        maxVelocity = conf_c.maxVelocity;
        maxForce = conf_c.maxForce;
        color = conf_c.color;
        offspringRange = conf_c.offspringRange;
        sex = conf_c.sex;
    }

    var dna = new DNA(
        initialRadius,
        maxVelocity,
        maxForce,
        color,
        initialDetectionRadius,
        offspringRange,
        sex
    );

    return new Carnivore(
        x, y, dna
    );
}


function generateHerbivore(x, y) { // Function to manually add more herbivores
    var initialRadius = generateNumberInRange(3, 8);
    var maxVelocity = generateNumberInRange(1, 2.2);
    var maxForce = generateNumberInRange(0.01, 0.05);
    var color = generateColor();
    var initialDetectionRadius = generateNumberInRange(40, 120);
    var minOffspring = generateInteger(1, 1);
    var maxOffspring = minOffspring + generateInteger(1, 8);
    var offspringRange = [minOffspring, maxOffspring];
    var sex;

    if (Math.random() < 0.5) {
        sex = 'XX';
    } else {
        sex = 'XY';
    }

    if (conf_h) {
        initialRadius = conf_h.initialRadius;
        maxVelocity = conf_h.maxVelocity;
        maxForce = conf_h.maxForce;
        color = conf_h.color;
        offspringRange = conf_h.offspringRange;
        sex = conf_h.sex;
    }

    var dna = new DNA(
        initialRadius,
        maxVelocity,
        maxForce,
        color,
        initialDetectionRadius,
        offspringRange,
        sex
    );

    return new Herbivore(
        x, y, dna
    );
}

function generateColor() {
    // Variables for color generation
    var r = Math.floor(Math.random() * 256);
    var g = Math.floor(Math.random() * 256);
    var b = Math.floor(Math.random() * 256);
    var color = "rgb(" + r + "," + g + "," + b + ")";

    return color;
}

function hexToRgb(hex) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ?
        "rgb(" +
        parseInt(result[1], 16) + "," +
        parseInt(result[2], 16) + "," +
        parseInt(result[3], 16) +
        ")"
        : null;
}

function rgbToHex(rgb) {
    let result = /^rgb\(([\d]{1,3}),([\d]{1,3}),([\d]{1,3})\)$/i.exec(rgb);
    if (!result) return null;

    let r = parseInt(result[1]).toString(16);
    let g = parseInt(result[2]).toString(16);
    let b = parseInt(result[3]).toString(16);

    return `#${r.length<2? "0"+r:r}${g.length<2? "0"+g:g}${b.length<2? "0"+b:b}`;
}

function colorMutation(style) {
    if (Math.random() < mutationProbability) { // The lower the mutationProbability, the lower the chance of mutation occurring
        let colors = style.substring(4, style.length - 1) // Remove text characters, e.g., "rgb(256,20,40)"
            .split(',') // Return an array with elements separated by commas, e.g., 256,20,40
            .map(function (color) { // Process each element of the array
                color = parseInt(color);
                let operation = "";
                let p = Math.random();

                if (color <= 10) { // To avoid generating negative numbers
                    operation = "addition";
                } else if (color >= 246) { // To avoid generating values greater than 256
                    operation = "subtraction";
                } else { // Randomly determine whether to add or subtract values if the color is between 10 and 246
                    if (Math.random() < 0.5) {
                        operation = "addition";
                    } else {
                        operation = "subtraction";
                    }
                }

                if (operation === "addition") {
                    if (p < 0.002) { // There is a 0.2% chance of a large mutation
                        return Math.ceil(color + color * (Math.random() * mutationMagnitude * 10));
                    } else if (p < 0.008) { // There is a 0.6% chance (0.8% - 0.2% from the previous if) of a reasonably large mutation
                        return Math.ceil(color + color * (Math.random() * mutationMagnitude * 4));
                    } else if (p < 0.028) { // There is a 2% chance (2.8% - 0.8% from the previous if) of a reasonable mutation
                        return Math.ceil(color + color * (Math.random() * mutationMagnitude * 2));
                    } else {
                        return Math.ceil(color + color * (Math.random() * mutationMagnitude));
                    }
                } else { // Subtraction
                    if (p < 0.002) { // There is a 0.2% chance of a large mutation
                        return Math.ceil(color - color * (Math.random() * mutationMagnitude * 10));
                    } else if (p < 0.008) { // There is a 0.6% chance (0.8% - 0.2% from the previous if) of a reasonably large mutation
                        return Math.ceil(color - color * (Math.random() * mutationMagnitude * 4));
                    } else if (p < 0.028) { // There is a 2% chance (2.8% - 0.8% from the previous if) of a reasonable mutation
                        return Math.ceil(color - color * (Math.random() * mutationMagnitude * 2));
                    } else {
                        return Math.ceil(color - color * (Math.random() * mutationMagnitude));
                    }
                }
            });

        // console.log("COLOR MUTATION");
        return `rgb(${colors[0]},${colors[1]},${colors[2]})`;
    } else {
        return style;
    }
}

function newMutation(value) { // Example: value = 20; mutationMagnitude = 0.05 or 5%
    if (Math.random() < mutationProbability) { // The lower the mutationProbability, the lower the chance of mutation occurring
        let p = Math.random();
        let variation = value * mutationMagnitude; // variation = 20 * 0.05 = 1, meaning it can vary from +1 to -1 in the result
        if (p < 0.001) { // There is a 0.1% chance of a very large mutation
            variation *= 10;
        } else if (p < 0.003) { // There is a 0.2% chance (0.3% - 0.1% from the previous if) of a large mutation
            variation *= 6;
        } else if (p < 0.008) { // There is a 0.5% chance (0.8% - 0.3% from the previous if) of a reasonably large mutation
            variation *= 3.5;
        } else if (p < 0.028) { // There is a 2% chance (2.8% - 0.8% from the previous if) of a reasonable mutation
            variation *= 2;
        }

        let minimum = value - variation; // minimum = 20 - 1 = 19. To avoid sub-dividing the return into addition or subtraction,
                                         // the reference point is shifted to the lowest possible value. Hence, the result will vary from
                                         // 0 to +2, as the distance from -1 to +1 is 2.
        if (minimum <= 0) {
            minimum = value * 0.01; // If the mutation reduces the value to less than 0, it will be a very small mutation
        }
        // console.log("MUTATION");
        return minimum + Math.random() * variation; // 19 + Math.random() * 2. The result will be within the range [19, 21]
    } else { // If no mutation occurs, return the original value
        return value;
    }
}

function nestedMutation(minNest, maxNest) {
    if (Math.random() < mutationProbability) { // The lower the mutationProbability, the lower the chance of mutation occurring
        let variationMinNest = generateInteger(0, 2 + Math.floor(mutationMagnitude * 10));
        let variationMaxNest = generateInteger(0, 2 + Math.floor(mutationMagnitude * 10));

        if (Math.random() >= 0.5) { // Addition
            minNest += variationMinNest;
            maxNest += variationMaxNest;
        } else { // Subtraction
            minNest -= variationMinNest;
            maxNest -= variationMaxNest;
        }

        if (minNest <= 0) {
            minNest = 0;
        }
        if (maxNest <= minNest) {
            maxNest = minNest + 1;
        }
    }

    return [minNest, maxNest];
}

function generateNumberInRange(min, max) {
    let delta = max - min; // Example: 4000 and 6000. 6000 - 4000 = 2000
    return parseFloat((Math.random() * delta + min).toFixed(4)); // Math.random() * 2000 + 4000
}

function createGradualFood() {
    if (!paused) { // Stop creating food while the simulation is paused
        if (splitScreen) {
            if (leftSideEmpty) { // If there is no population on the left side, it will not generate food there
                var x = generateNumberInRange(universeWidth / 2 + 31, universeWidth - 31);
                var y = Math.random() * (universeHeight - 62) + 31;
                var radius = Math.random() * 1.5 + 1;

                if (Food.foods.length < 3000) { // Limit to avoid overloading the simulation
                    new Food(x, y, radius);
                }
            }
            if (rightSideEmpty) { // If there is no population on the right side, it will not generate food there
                var x = generateNumberInRange(31, universeWidth / 2 - 31);
                var y = Math.random() * (universeHeight - 62) + 31;
                var radius = Math.random() * 1.5 + 1;

                if (Food.foods.length < 3000) { // Limit to avoid overloading the simulation
                    new Food(x, y, radius);
                }
            }
            if (!rightSideEmpty && !leftSideEmpty) {
                var x = Math.random() * (universeWidth - 62) + 31;
                var y = Math.random() * (universeHeight - 62) + 31;
                var radius = Math.random() * 1.5 + 1;

                if (Food.foods.length < 3000) { // Limit to avoid overloading the simulation
                    new Food(x, y, radius);
                }
            }
        } else {
            var x = Math.random() * (universeWidth - 62) + 31;
            var y = Math.random() * (universeHeight - 62) + 31;
            var radius = Math.random() * 1.5 + 1;

            if (Food.foods.length < 3000) { // Limit to avoid overloading the simulation
                new Food(x, y, radius);
            }
        }
    }
}

function changeFoodCreationInterval(newValue, create = false) {
    newTime = 1000 / newValue;
    if (!create) {
        clearInterval(foodCreationInterval);
    }
    if (newTime > 1000) return;
    if (beforePlay) return;
    foodCreationInterval = setInterval(createGradualFood, newTime);
}

function changeMutationProbability(newValue) {
    mutationProbability = newValue / 100;
}

function changeMutationMagnitude(newValue) {
    mutationMagnitude = newValue / 100;
}

function drawDivision() {
    context.beginPath();
    context.moveTo(universeWidth / 2, 0);
    context.lineTo(universeWidth / 2, universeHeight);
    context.strokeStyle = "white";
    context.stroke();
}

function drawQuadTree(qtree) {
    qtree.draw();

    let range = new Rectangle(Math.random() * universeWidth, Math.random() * universeHeight, 170, 123);
    context.rect(range.x - range.width, range.y - range.height, range.width * 2, range.height * 2);
    context.strokeStyle = "green";
    context.lineWidth = 3;
    context.stroke();

    let points = qtree.query(range);
    for (let p of points) {
        context.beginPath();
        context.arc(p.x, p.y, 1, 0, 2 * Math.PI);
        context.strokeStyle = "red";
        context.stroke();
    }
}

function createPoints() {
    let congregation = new Point(Math.random() * universeWidth, Math.random() * universeHeight);
    
    for (var i = 0; i < 500; i++) {
        let p = new Point(Math.random() * universeWidth, Math.random() * universeHeight);
        qtree.insertPoint(p);
    }
    for (var i = 0; i < 300; i++) {
        let p = new Point(congregation.x + (Math.random() - 0.5) * 300, congregation.y + (Math.random() - 0.5) * 300);
        qtree.insertPoint(p);
    }
    for (var i = 0; i < 400; i++) {
        let p = new Point(congregation.x + (Math.random() - 0.5) * 600, congregation.y + (Math.random() - 0.5) * 600);
        qtree.insertPoint(p);
    }
    for (var i = 0; i < 400; i++) {
        let p = new Point(congregation.x + (Math.random() - 0.5) * 800, congregation.y + (Math.random() - 0.5) * 800);
        qtree.insertPoint(p);
    }
}

function calculateChartStats() {
    // Clear memory space of previous variables
    popH = velAvgH = forceAvgH = radiusAvgH = detectionRadiusAvgH = energyAvgH = energyConsumptionRateAvgH = offspringAvgH = null;
    popC = velAvgC = forceAvgC = radiusAvgC = detectionRadiusAvgC = energyAvgC = energyConsumptionRateAvgC = offspringAvgC = null;

    // Reset variables for herbivores
    popH = { no_div: 0, left: 0, right: 0 };
    velAvgH = { no_div: 0, left: 0, right: 0 };
    forceAvgH = { no_div: 0, left: 0, right: 0 };
    radiusAvgH = { no_div: 0, left: 0, right: 0 };
    detectionRadiusAvgH = { no_div: 0, left: 0, right: 0 };
    energyAvgH = { no_div: 0, left: 0, right: 0 };
    energyConsumptionRateAvgH = { no_div: 0, left: 0, right: 0 };
    offspringAvgH = { no_div: 0, left: 0, right: 0 };

    // Reset variables for carnivores
    popC = { no_div: 0, left: 0, right: 0 };
    velAvgC = { no_div: 0, left: 0, right: 0 };
    forceAvgC = { no_div: 0, left: 0, right: 0 };
    radiusAvgC = { no_div: 0, left: 0, right: 0 };
    detectionRadiusAvgC = { no_div: 0, left: 0, right: 0 };
    energyAvgC = { no_div: 0, left: 0, right: 0 };
    energyConsumptionRateAvgC = { no_div: 0, left: 0, right: 0 };
    offspringAvgC = { no_div: 0, left: 0, right: 0 };

    Herbivore.herbivores.forEach(herbivore => {
        // Sum the values of variables for all herbivores
        popH["no_div"]++;
        velAvgH["no_div"] += herbivore.maxSpeed;
        forceAvgH["no_div"] += herbivore.maxForce;
        radiusAvgH["no_div"] += herbivore.initialRadius * 1.5; // Maximum radius is (1.5 * initialRadius)
        detectionRadiusAvgH["no_div"] += herbivore.initialDetectionRadius * 1.3; // 1.3 instead of 1.5 because detection radius increases less than the radius
        energyAvgH["no_div"] += herbivore.maxFixedEnergy;
        energyConsumptionRateAvgH["no_div"] += herbivore.maxEnergyConsumptionRate;
        offspringAvgH["no_div"] += (herbivore.offspringInterval[0] + herbivore.offspringInterval[1]) / 2;

        if (splitScreen) {
            // Check if it's on the left or right side
            let side;
            if (herbivore.position.x < universeWidth / 2) {
                side = "left";
            } else {
                side = "right";
            }
            // Sum the values of variables for herbivores on each side
            popH[side]++;
            velAvgH[side] += herbivore.maxSpeed;
            forceAvgH[side] += herbivore.maxForce;
            radiusAvgH[side] += herbivore.initialRadius * 1.5; // Maximum radius is (1.5 * initialRadius)
            detectionRadiusAvgH[side] += herbivore.initialDetectionRadius * 1.3; // 1.3 instead of 1.5 because detection radius increases less than the radius
            energyAvgH[side] += herbivore.maxFixedEnergy;
            energyConsumptionRateAvgH[side] += herbivore.maxEnergyConsumptionRate;
            offspringAvgH[side] += (herbivore.offspringInterval[0] + herbivore.offspringInterval[1]) / 2;
        }
    });

    // Translation for carnivores is missing from the provided code, please provide the translation for it if needed.
}


Carnivore.carnivores.forEach(carnivore => {
    // Sum the values of variables for all carnivores
    popC["no_div"]++;
    velAvgC["no_div"] += carnivore.maxSpeed;
    forceAvgC["no_div"] += carnivore.maxForce;
    radiusAvgC["no_div"] += carnivore.initialRadius * 1.5; // Maximum radius is (1.5 * initialRadius)
    detectionRadiusAvgC["no_div"] += carnivore.initialDetectionRadius * 1.3; // 1.3 instead of 1.5 because detection radius increases less than the radius
    energyAvgC["no_div"] += carnivore.maxFixedEnergy;
    energyConsumptionRateAvgC["no_div"] += carnivore.maxEnergyConsumptionRate;
    offspringAvgC["no_div"] += (carnivore.offspringInterval[0] + carnivore.offspringInterval[1]) / 2;

    if (splitScreen) {
        // Check if it's on the left or right side
        let side;
        if (carnivore.position.x < universeWidth / 2) {
            side = "left";
        } else {
            side = "right";
        }
        // Sum the values of variables for carnivores on each side
        popC[side]++;
        velAvgC[side] += carnivore.maxSpeed;
        forceAvgC[side] += carnivore.maxForce;
        radiusAvgC[side] += carnivore.initialRadius * 1.5; // Maximum radius is (1.5 * initialRadius)
        detectionRadiusAvgC[side] += carnivore.initialDetectionRadius * 1.3; // 1.3 instead of 1.5 because detection radius increases less than the radius
        energyAvgC[side] += carnivore.maxFixedEnergy;
        energyConsumptionRateAvgC[side] += carnivore.maxEnergyConsumptionRate;
        offspringAvgC[side] += (carnivore.offspringInterval[0] + carnivore.offspringInterval[1]) / 2;
    }        
});


// Divide the total sum by the number of herbivores to obtain the average
// No division
velAvgH.no_div /= popH.no_div;
forceAvgH.no_div /= popH.no_div;
radiusAvgH.no_div /= popH.no_div;
detectionRadiusAvgH.no_div /= popH.no_div;
energyAvgH.no_div /= popH.no_div;
energyConsumptionRateAvgH.no_div /= popH.no_div;
offspringAvgH.no_div /= popH.no_div;
// Left side
velAvgH.left /= popH.left;
forceAvgH.left /= popH.left;
radiusAvgH.left /= popH.left;
detectionRadiusAvgH.left /= popH.left;
energyAvgH.left /= popH.left;
energyConsumptionRateAvgH.left /= popH.left;
offspringAvgH.left /= popH.left;
// Right side
velAvgH.right /= popH.right;
forceAvgH.right /= popH.right;
radiusAvgH.right /= popH.right;
detectionRadiusAvgH.right /= popH.right;
energyAvgH.right /= popH.right;
energyConsumptionRateAvgH.right /= popH.right;
offspringAvgH.right /= popH.right;

// Divide the total sum by the number of carnivores to obtain the average
// No division
velAvgC.no_div /= popC.no_div;
forceAvgC.no_div /= popC.no_div;
radiusAvgC.no_div /= popC.no_div;
detectionRadiusAvgC.no_div /= popC.no_div;
energyAvgC.no_div /= popC.no_div;
energyConsumptionRateAvgC.no_div /= popC.no_div;
offspringAvgC.no_div /= popC.no_div;
// Left side
velAvgC.left /= popC.left;
forceAvgC.left /= popC.left;
radiusAvgC.left /= popC.left;
detectionRadiusAvgC.left /= popC.left;
energyAvgC.left /= popC.left;
energyConsumptionRateAvgC.left /= popC.left;
offspringAvgC.left /= popC.left;
// Right side
velAvgC.right /= popC.right;
forceAvgC.right /= popC.right;
radiusAvgC.right /= popC.right;
detectionRadiusAvgC.right /= popC.right;
energyAvgC.right /= popC.right;
energyConsumptionRateAvgC.right /= popC.right;
offspringAvgC.right /= popC.right;



function checkDividedPopulations() {
    if (splitScreen) {
        rightSideEmpty = true;
        leftSideEmpty = true;

        Herbivore.herbivores.forEach(herbivore => {
            // Check left side
            if (herbivore.position.x < universeWidth / 2 - 31) {
                leftSideEmpty = false;
            }

            // Check right side
            if (herbivore.position.x > universeWidth / 2 + 31) {
                rightSideEmpty = false;
            }
        });
    }
}

let animateId;

function pause() {
    paused = true;

    pauseBtn.classList.add("d-none");
    resumeBtn.classList.remove("d-none");
}

function resume() {
    paused = false;

    resumeBtn.classList.add("d-none");
    pauseBtn.classList.remove("d-none");

    animate();
}

function accelerate() {
    animate();

    // decelerateBtn.classList.remove("d-none");
}

function decelerate() {
    pause();
    setTimeout(resume, 10);
}

function animate() {
    if (!paused) {
        animateId = requestAnimationFrame(animate);
    }

    context.clearRect(0, 0, universeWidth, universeHeight);
    context.beginPath();
    context.moveTo(-3, -4);
    context.lineTo(universeWidth + 3, -3);
    context.lineTo(universeWidth + 3, universeHeight + 3);
    context.lineTo(-3, universeHeight + 3);
    context.lineTo(-3, -3);
    context.strokeStyle = "white";
    context.stroke();

    // Creating the Quadtree
    let quadtree = new QuadTree(canvasRectangle, 10);

    // Screen Splitting
    if (splitCheckbox.checked) {
        splitScreen = true;
    } else {
        splitScreen = false;
    }

    if (splitScreen) {
        drawSplit();

        Food.foods.forEach((food, i) => {
            food.display();
            // Remove foods near the split to prevent organisms from being attracted to it
            if (food.position.x - universeWidth / 2 < 30 && food.position.x - universeWidth / 2 > -30) {
                Food.foods.splice(i, 1);
            }

            quadtree.insertFood(food); // Insert food into the Quadtree
        });

        if (loopLimiter < 10) {
            loopLimiter++;
        }
    

        
        Organism.organisms.forEach((organism) => {
            if (organism.position.x <= universeWidth / 2) { // if the organism is on the left side
                if (loopLimiter == 1 && universeWidth / 2 - organism.position.x < 10) { // push organisms close to the edge to the side
                    organism.position.x -= 10;
                }
                organism.createBorders(true); // splitScreen: true
            } else { // if the organism is on the right side
                if (loopLimiter == 1 && organism.position.x - universeWidth / 2 < 10) { // push organisms close to the edge to the side
                    organism.position.x += 10;
                }
                organism.createBorders(true); // splitScreen: true
            }
        });

        // Inserting organisms into the QuadTree before calling their methods
        Herbivore.herbivores.forEach(herbivore => {
            quadtree.insertHerbivore(herbivore); // Insert herbivore into the QuadTree
        });

        Carnivore.carnivores.forEach(carnivore => {
            quadtree.insertCarnivore(carnivore); // Insert carnivore into the QuadTree
        });

        // Calling methods of organisms
        Herbivore.herbivores.forEach(herbivore => {
            herbivore.update();
            herbivore.wander();

            // Convert the detection radius into a circle object for manipulation
            let herbivoreVision = new Circle(herbivore.position.x, herbivore.position.y, herbivore.detectionRadius);

            // herbivore.searchForFood(quadtree, herbivoreVision);
            if (herbivore.energy <= herbivore.maxEnergy * hungerThreshold_h) { // HUNGER
                herbivore.searchForFood(quadtree, herbivoreVision);
            }
            herbivore.detectPredator(quadtree, herbivoreVision);
        });

        Carnivore.carnivores.forEach(carnivore => {
            carnivore.update();
            carnivore.wander();

            // Convert the detection radius into a circle object for manipulation
            let carnivoreVision = new Circle(carnivore.position.x, carnivore.position.y, carnivore.detectionRadius);

            // carnivore.searchForHerbivore(quadtree, carnivoreVision);
            if (carnivore.energy <= carnivore.maxEnergy * hungerThreshold_c) { // HUNGER
                carnivore.searchForHerbivore(quadtree, carnivoreVision);
            }
        });

    } else { // if the screen is NOT split
        loopLimiter = 0;

        Food.foods.forEach(food => {
            food.display();
            quadtree.insertFood(food); // Insert food into the QuadTree
        });

        Organism.organisms.forEach((organism) => {
            organism.createBorders(false); // splitScreen: false
        });
        


        // Inserting organisms into the QuadTree before calling their methods
        Herbivore.herbivores.forEach(herbivore => {
            quadtree.insertHerbivore(herbivore); // Insert herbivore into the QuadTree
        });

        Carnivore.carnivores.forEach(carnivore => {
            quadtree.insertCarnivore(carnivore); // Insert carnivore into the QuadTree
        });

        // Calling methods of organisms
        Herbivore.herbivores.forEach(herbivore => {
            herbivore.update();
            herbivore.wander();

            // Convert the detection radius into a circle object for manipulation
            let visionH = new Circle(herbivore.position.x, herbivore.position.y, herbivore.detectionRadius);

            // herbivore.searchForFood(quadtree, visionH);
            if (herbivore.energy <= herbivore.maxEnergy * hungerThreshold_h) { // HUNGER
                herbivore.searchForFood(quadtree, visionH);
            }

            herbivore.detectPredator(quadtree, visionH);
        });

        Carnivore.carnivores.forEach(carnivore => {
            carnivore.update();
            carnivore.wander();

            // Convert the detection radius into a circle object for manipulation
            let visionC = new Circle(carnivore.position.x, carnivore.position.y, carnivore.detectionRadius);

            // carnivore.searchForHerbivore(quadtree, visionC);
            if (carnivore.energy <= carnivore.maxEnergy * hungerThreshold_c) { // HUNGER
                carnivore.searchForHerbivore(quadtree, visionC);
            }
        });

    }
}
    

function generateRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }

            // ----------------------------------------------------------------------------------------------
            //                            Dynamic Panels and Popovers
            // ----------------------------------------------------------------------------------------------
            // Function attached to the click event to find the organism in the list and return its properties
function getOrganism(x, y) {
    let organism = Organism.organisms.find(o => Math.abs(o.position.x - x) <= 10 && Math.abs(o.position.y - y) <= 10);
    if (organism === undefined) {
        return; //console.log("not found")
    }

    // Check if a popover for the clicked organism already exists
    let popoverExists = document.querySelectorAll(`.popover-info[data-organismid="${organism.id}"]`).length > 0 ? 1 : 0;
    if (popoverExists) {
        return;
    }

    let popover = `
        <div id="popover-${popover_id}" class="popover-info" data-organismid="${organism.id}" style="top:${parseInt(organism.position.y - 20)}px; left:${parseInt(organism.position.x + 15)}px">
            <div class="popover-title">
                ${(organism instanceof Carnivore) ? "Carnivore" : "Herbivore"} <div style="color: grey; display: inline; font-size: medium">#${organism.id}</div>
            </div>
            <div class="popover-content">
                <b>Gender:</b> <div id="pop-sex-${popover_id}" style="display: inline">${organism.gender}</div><br/>
                <b>Radius:</b> <div id="pop-radius-${popover_id}" style="display: inline">${organism.radius.toFixed(2)}</div>/${(organism.initialRadius * 1.5).toFixed(2)}<br/>
                <b>Velocity:</b> <div id="pop-velocity-${popover_id}" style="display: inline">${organism.velocity.mag().toFixed(2)}</div>/${organism.maxVelocity.toFixed(2)}<br/>
                <b>Detection Radius:</b> <div id="pop-detection-${popover_id}" style="display: inline">${organism.detectionRadius.toFixed(2)}</div><br/>
                <b>Energy:</b> <div id="pop-energy-${popover_id}" style="display: inline">${organism.energy.toFixed(2)}</div>/<div id="pop-max-energy-${popover_id}" style="display: inline">${organism.maxEnergy.toFixed(2)}</div><br/>
                <b>Energy Expenditure:</b> <div id="pop-expenditure-${popover_id}" style="display: inline">${(organism.energyExpenditureRate + organism.minEnergyExpenditure).toFixed(3)}</div><br/>
                <b>Color:</b> <svg width="20" height="20"><rect width="18" height="18" style="fill:${organism.color}"/></svg> ${organism.color}<br/>
                <b>Status:</b> <div id="pop-status-${popover_id}" style="display: inline">${organism.status}</div><br/>
                <b>Age:</b> <div id="pop-lifespan-${popover_id}" style="display: inline">${organism.lifespan}</div>/${organism.maxLifespan}<br/>
                <b>Offspring Interval:</b> from <div id="pop-min-offspring-${popover_id}" style="display: inline">${organism.offspringInterval[0]}</div> to <div id="pop-max-offspring-${popover_id}" style="display: inline">${organism.offspringInterval[1]}</div><br/>
                <b>Offspring Count:</b> <div id="pop-reproduced-${popover_id}" style="display: inline">${organism.offspring.length}</div><br/>
                <button type="button" class="btn btn-danger btn-sm" onclick="deleteOrganismPopover(${popover_id}, ${organism.id})" style="margin-top: 10px">Delete ${(organism instanceof Carnivore) ? "Carnivore" : "Herbivore"}</button>
            </div>
            <button type="button" class="btn close" aria-label="Close"
                onclick="deletePopover(${popover_id}, ${organism.id})">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    
    `
    // Append the popover to the body
    $("body").append($(popover));

    let pop_id = popover_id;
    // Create a proxy to monitor changes in the position property of the organism
    organism.proxy = new Proxy(organism.position, {
        set: function (target_org, key_position, value) {
            target_org[key_position] = value;

            let cssProperty = key_position == "x" ?
                { left: parseInt((value + 15 - context.transformedPoint(0, 0).x)) } :
                { top: parseInt(value - 20 - context.transformedPoint(0, 0).y) };

            // Make the popover follow the position of the organism
            $(`#popover-${pop_id}`).css(cssProperty);
            document.getElementById(`pop-radius-${pop_id}`).textContent = organism.radius.toFixed(1);
            document.getElementById(`pop-velocity-${pop_id}`).textContent = organism.velocity.mag().toFixed(2);
            document.getElementById(`pop-detection-${pop_id}`).textContent = organism.detectionRadius.toFixed(2);
            document.getElementById(`pop-energy-${pop_id}`).textContent = organism.energy.toFixed(1);
            document.getElementById(`pop-max-energy-${pop_id}`).textContent = organism.maxEnergy.toFixed(1);
            document.getElementById(`pop-expenditure-${pop_id}`).textContent = (organism.energyExpenditureRate + organism.minEnergyExpenditure).toFixed(3);
            document.getElementById(`pop-status-${pop_id}`).textContent = organism.status;
            document.getElementById(`pop-reproduced-${pop_id}`).textContent = organism.offspring.length;
            document.getElementById(`pop-lifespan-${pop_id}`).textContent = organism.lifespan;
            document.getElementById(`pop-min-offspring-${pop_id}`).textContent = organism.offspringInterval[0];
            document.getElementById(`pop-max-offspring-${pop_id}`).textContent = organism.offspringInterval[1];
            return true;
        }
    });

    // Save the popover ID in the organism
    organism.popover_id = pop_id;
    popover_id++;
}

function deletePopover(popoverId, organismId) {
    // Capture the organism
    const organism = Organism.organisms.find(o => o.id == organismId) || 0;
    if (organism) {
        delete organism.proxy;
        delete organism.popover_id;
    }
    $(`#popover-${popoverId}`).remove();

    // Hide the close button if there are no more popovers open
    if ($(".popover-info").length == 0) {
        $("#btnDeletePopovers").hide();
    }
}

function deleteOrganismPopover(popoverId, organismId) {
    // Capture the organism
    const organism = Organism.organisms.find(o => o.id == organismId) || 0;
    if (organism) {
        organism.die();
        if (paused) {
            unpause(); // If not done, the organism will continue to appear while paused
            pause();
        }
    }
    $(`#popover-${popoverId}`).remove();
}

// Display the edit panel for choosing properties of the added organisms
function showEditPanel(type) {
    // Restore saved configurations
    let config;
    if (type == 1) {
        config = conf_c;
    } else {
        config = conf_h;
    }


    let panel = `
    
        <div class="row mb-3">
            <div id="edit-title" class="col-8">${type == 1 ? "Carnivore" : "Herbivore"}</div>
            <!-- Disable all inputs if random is enabled -->
            <button id="edit-random" class="btn col-2 btn-gray" onclick="randomConfig(${type})"><i class="fas fa-dice"></i></button>
            <button class="btn close col-2" onclick="$(this).closest('.edit-organism').addClass('d-none').html('')">
                <span class="text-white" aria-hidden="true">&times;</span>
            </button>
        </div>
    

        <form id="formConfig" class="container-fluid">
            <div class="row mb-3">
                <div style="display: inline; width: 50%">
                    <!-- Organism drawing with real-time update -->
                    <b><label for="input-color">Color</label></b>
                    <input id="input-color" name="color" type="color" value="${config ? rgbToHex(config.color) : "#ff0000"}">
                </div>
            </div>
            <div class="row p-0">
                <div style="display: inline; width: 50%">
                    <b><label for="input-radius">Radius</label></b>
                    <input id="input-radius" name="initial_radius" type="number" value="${config ? config.initial_radius : (initial_radius || generateNumberInRange(3, 7).toFixed(2))}" class="form-control p-0">
                </div>  
                <div style="display: inline; width: 50%">                 
                    <b><label for="input-velocity">Velocity</label></b>
                    <input id="input-velocity" name="max_velocity" type="number" value="${config ? config.max_velocity.toFixed(2) : generateNumberInRange(1, 2.2).toFixed(2)}" class="form-control p-0">
                </div>
                <div style="display: inline; width: 50%">
                    <b><label for="input-agility">Agility</label></b>
                    <input id="input-agility" name="max_agility" type="number" value="${config ? config.max_agility.toFixed(2) : generateNumberInRange(0.001, 0.05).toFixed(2)}" class="form-control p-0">
                </div>
                <div style="display: inline; width: 50%">
                    <b><label for="input-detection">Vision</label></b>
                    <input id="input-detection" name="initial_detection_radius" type="number" value="${config ? config.initial_detection_radius.toFixed(2) : generateNumberInRange(15, 60).toFixed(2)}" class="form-control p-0">
                </div>
                <div style="display: inline; width: 50%">
                    <b><label for="input-litter">Litter Size</label></b>
                    <input id="input-litter-min" name="min_litter_interval" type="number" value="${config ? config.litter_interval[0] : generateNumberInRange(1, 5)}" class="form-control p-0">
                    <input id="input-litter-max" name="max_litter_interval" type="number" value="${config ? config.litter_interval[1] : generateNumberInRange(1, 5)}" class="form-control p-0">
                    </div>
            </div>
        </form>
        <div class="row mt-2">
            <button type="button" onclick="serializeFormConfig(${type})" class="btn btn-sm btn-outline-secondary btn-block">Save</button>
        </div>
    
    
    `
    $("#editPanel").html(panel).removeClass("d-none")
    // Start as random if no previous saved configuration exists
    if (!config) {
        randomConfig(type);
    }
}

function serializeFormConfig(type) {
    let obj = $("#formConfig").serializeArray().reduce(function(obj, value, i) {
        obj[value.name] = value.value;
        return obj;
    }, {});
    // Convert color
    obj.color = hexToRgb(obj["color"])
    
    // Convert numbers
    obj.initial_radius = parseFloat(obj.initial_radius);
    obj.max_velocity = parseFloat(obj.max_velocity);
    obj.max_agility = parseFloat(obj.max_agility);
    obj.initial_detection_radius = parseFloat(obj.initial_detection_radius);
    obj.litter_interval = [parseFloat(obj.min_litter_interval), parseFloat(obj.max_litter_interval)];

    if (type == 1) {
        conf_c = obj;
    } else {
        conf_h = obj;
    }
}


function randomConfig(type) {
    if ($("#edit-random").hasClass("active")) {
        $("#edit-random").removeClass("active");

        // Enable inputs
        $("#formConfig input").prop("disabled", false);
    } else {
        // Reset configuration
        if (type == 1 && conf_c) {
            // TODO: Show confirmation dialog to confirm randomization
            let result = confirm("By randomizing the values, you will lose the saved configurations for Carnivores. Do you want to continue?");
            if (result == true)
                conf_c = undefined;
            else
                return;
        } else if (type == 2 && conf_h) {
            // TODO: Show confirmation dialog to confirm randomization
            let result = confirm("By randomizing the values, you will lose the saved configurations for Herbivores. Do you want to continue?");
            if (result == true)
                conf_h = undefined;
            else
                return;
        }
        $("#edit-random").addClass("active");
        // Disable configuration inputs
        $("#formConfig input").prop("disabled", true);
    }
}

// ----------------------------------------------------------------------------------------------
//                                         Timer
// ----------------------------------------------------------------------------------------------
var timer;

function createTimer() {
    timer = setInterval(() => { updateTimer(); }, 10);
}

function updateTimer() {
    if (!paused) { // Only update if the simulation is not paused
        if ((milliseconds += 10) == 1000) {
            milliseconds = 0;
            seconds++;
            total_seconds++;
        }
        if (seconds == 60) {
            seconds = 0;
            minutes++;
        }
        if (minutes == 60) {
            minutes = 0;
            hours++;
        }
        document.getElementById('hour').innerText = formatTime(hours);
        document.getElementById('minute').innerText = formatTime(minutes);
        document.getElementById('second').innerText = formatTime(seconds);
        document.getElementById('millisecond').innerText = formatTime(milliseconds);
    }
}

function formatTime(input) {
    return input > 10 ? input : `0${input}`;
}

function resetTimer() {
    hours = minutes = seconds = milliseconds = total_seconds = 0;

    // Clear the timer if it exists.
    try {
        clearInterval(timer);
    } catch (e) {}

    document.getElementById('hour').innerText = "00";
    document.getElementById('minute').innerText = "00";
    document.getElementById('second').innerText = "00";
    document.getElementById('millisecond').innerText = "00";
}

function generateId(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


// ----------------------------------------------------------------------------------------------
//                                         Frame rate
// ----------------------------------------------------------------------------------------------

// // The higher this value, the less the fps will reflect temporary variations
// // A value of 1 will only keep the last value
// var filterStrength = 20;
// var frameTime = 0, lastLoop = new Date, thisLoop;

// function gameLoop(){
//   // ...
//   var thisFrameTime = (thisLoop=new Date) - lastLoop;
//   frameTime+= (thisFrameTime - frameTime) / filterStrength;
//   lastLoop = thisLoop;
// }

// // Report the fps only every second, to only lightly affect measurements
// var fpsOut = document.getElementById('framerate');
// setInterval(function(){
//   fpsOut.innerHTML = parseFloat((1000/frameTime).toFixed(1)) + " fps";
// },500);

// // function calculaFrameRate(){
// //     var fps;
// //     var thisLoop = new Date();
// //     fps = 1000/(thisLoop - lastLoop);
// //     lastLoop = thisLoop;

// //     return fps;
// //     document.getElementById("framerate").innerHTML = fps;
// // }

// setInterval(() => {
//     var thisLoop = new Date();
//     var fps = 1000/(thisLoop - lastLoop);
//     lastLoop = thisLoop;

//     document.getElementById("framerate").innerHTML = fps;
// }, 1000);
