# Evolution Simulation

The project uses AI algorithms to show how organisms evolve and mutate through generations.The project consists of two type of creatures that are herbivores(prey) and carnivores(preditors) in a 2D world. Both evolve as per their population for survival which is demonstrated through various real time plots on various traits.


## Programming Languages & UI used 
html(hosting) , java script(souce code) , css(styling) , canvas(UI)
## Installation

To download this project in your chosen folder type this is cmd.

```bash
 chosen folder>git clone https://gitlab.com/Shreya0511/natural_selection_simulation.git
```
if it shows error do this first

```bash
 pip install npm
```
after Installation run the "index.html" file to see how simulation works

